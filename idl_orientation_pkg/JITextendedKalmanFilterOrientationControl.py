

import numpy as np


# Import Drone config class
from idl_botsy_pkg.JITdroneConfiguration import DroneGeometry
from idl_orientation_pkg.JITextendedKalmanFilterParameters import InsParameters


# import Numba stuff
from numba.experimental import jitclass
from numba import int32, float32, jit, types, typed, typeof  # import the types



# Numba specs for InsOrientationAtan
droneGeomNumbaType = DroneGeometry.class_type.instance_type
InsOrientationAtanSpecs = [
    ('__nStates', int32),
    ('__timeLastImuMsg', float32),
    ('__maxDt', float32),
    ('__accHighThreshold', float32),
    ('__gravity', float32),
    ('__yawThreshold', float32),
    ('__stateInitialization', float32), # TBD, just 0 as of now in the code
    ('__x', float32[:,:]),
    ('__xInit', float32[:,:]),
    ('__w', float32[:,:]),
    ('__P', float32[:,:]),
    ('__PInit', float32[:,:]),
    ('__Q', float32[:,:]),
    ('__R_yaw', float32),
    ('__R_leveling', float32),
    ('__hLeveling', float32[:,:]),
    ('__hYaw', float32[:,:]),
    ('__droneGeom', droneGeomNumbaType),
    ('__rotMat_bs', float32[:,:]),
    ('__t_nb', float32[:,:]),
]

# Orientation EKF
@jitclass(InsOrientationAtanSpecs)
class InsOrientationAtan(object):

    # Dt as input parameter is used to keep it in the same interface as other implementations
    def __init__(self, dt):

        ## System Number of states
        self.__nStates = 6

        ## Filter settings
        # Sets how much the abs acc measurement can deviate from g before switching to high g measurement function
        self.__accHighThreshold = 0.01     # percent of g
        self.__gravity = 9.81

        self.__yawThreshold = 1.57

        # Initialization of filter
        self.__stateInitialization = 0
        
        ## System state vector
        # [  0,     1,   2,  3,  4,  5]
        # [phi, theta, psi, pb, qb, rb]
        self.__x = np.zeros((self.__nStates,1), dtype=np.float32)
        self.__xInit = self.__x

        # Omega
        self.__w = np.zeros((3,1), dtype = np.float32)
        
        ## Predict and state covariance
        # System state covariance matrix
        self.__P = np.eye(self.__nStates, dtype=np.float32)
        self.__PInit = self.__P

        ## System state predict uncertainty matrix
        self.__Q = np.eye(self.__nStates, dtype=np.float32)
        

        ## Measurement uncertainties covariance
        # Matrixes
        self.__R_yaw        = 1.0
        self.__R_leveling   = 1.0

        # Measurement functions
        self.__hLeveling = np.zeros((2,self.__nStates), dtype = np.float32)
        self.__hLeveling[0,0] = 1.0
        self.__hLeveling[1,1] = 1.0

        self.__hYaw = np.zeros((1,self.__nStates), dtype = np.float32)
        self.__hYaw[0,2] = 1.0

        ## Geometry and transforms
        # Creates a Drone geometry object
        self.__droneGeom = DroneGeometry()

        # Transform from sensor to estimation frame (body/Ned typically)
        self.__rotMat_bs = np.eye(3, dtype = np.float32)
        self.__t_nb = np.zeros((3,3), dtype = np.float32)

    def predict(self, online = True):
        '''
            Dummy predict to keep the interface lords happy
        '''
        pass

    def __predict(self, dt, online = True):
        '''
            Used to predict the next filter state

            Input   dt is a scalar
        '''

        ## Computes transform to save on comp time Transforms
        # Rate transform from body to global
        tHeta = self.__x[0:3]
        self.__t_nb = self.__droneGeom.rateTransform_nb(tHeta)

        # Calculates curret linearization of the state transition equation
        F = self.__stateTransitionLin(dt, online = online)

        B = self.__controlInput(dt, online = online)

        W = self.__covariancePredictWeight(dt)

        # Predicts state
        self.__x = F @ self.__x + B @ self.__w

        # Predicts state covariance matrix
        self.__P = F @ self.__P @ F.T + W @ self.__Q @ W.T

        # Wrapping euler angles
        self.__wrapEulerAngles()

    def __stateTransitionLin(self, dt, online = True):
        '''
            Function to return state transition matrix

            Returns
                np shape (6,6)
        '''

        ## Transforms
        # Rate transform from body to global
        tHeta = self.__x[0:3]
        t_nb = self.__droneGeom.rateTransform_nb(tHeta)

        ## State transition
        F = np.eye(self.__nStates, dtype=np.float32)
        
        # Angle related
        F[0:3, 3:6] = -t_nb*dt

        if online == False:
            F[2,3:6] = np.zeros((1,3), dtype = np.float32)

        return F

    def __controlInput(self, dt, online = True):
        '''
            Function to return control input matrix

            Returns
                np shape (6, 3)
        '''

        # Predefining B matrix
        B = np.zeros((self.__nStates,3), dtype = np.float32)

        ## Transforms
        # Rate transform from body to global
        tHeta = self.__x[0:3]
        t_nb = self.__droneGeom.rateTransform_nb(tHeta)

        # Populates B matrix
        B[0:3,0:3] = t_nb * dt

        if online == False:
            B[2,0:3] = np.zeros((1,3), dtype = np.float32)

        return B

    def __covariancePredictWeight(self, dt):
        '''
            Function to return covariance weight update

            return W np shape (6,6)
        '''

        # Creates eye mat
        W = np.eye(self.__nStates, dtype = np.float32)

        ## Transforms
        # Rate transform from body to global
        tHeta = self.__x[0:3]
        t_nb = self.__droneGeom.rateTransform_nb(tHeta)

        # Sets nonlinear therm
        W[0:3, 0:3] = t_nb

        # Weights by dt
        W = W*dt

        return W

    def __wrapEulerAngles(self):
        '''
        Function to wrap states
        phi is wrapped to [-pi, pi)
        theta is wrapped to [-pi, pi)
        psi is wrapped to [0, 2*pi)
        '''

        ## Setting to state vector
        # Phi
        self.__x[0, 0] = self.__modPiToPi(self.__x[0,0])
        # Theta
        self.__x[1, 0] = self.__modPiToPi(self.__x[1,0])
        # Psi
        self.__x[2, 0] = np.mod(self.__x[2,0], 2*np.pi)

    def __modPiToPi(self, ang):
        '''
        Function to map a variable to [-pi to pi)

        TODO: Needs a way to handle cases where the angle is grossly wrong
        '''

        # Predefining variable
        angWrapped = 0.0

        # Wrapping
        if ang >= np.pi:
            angWrapped = ang-2*np.pi
        elif ang < -np.pi:
            angWrapped = 2*np.pi+ang
        else:
            angWrapped = ang

        return angWrapped

    def __innovationLin(self, xMeasure, H):
        '''
        Finds the error between the measurement and the predicted value, given a linear measurement function

        Input is    X_measure np shape (m,1)
                    H np shape (m,nStates)

        Output is   Y np shape (m,1)
        '''
        

        return xMeasure - H @ self.__x

    def __innovationYaw(self, yawMeasure, yawPredict):
        '''
        Function to return "geodesic" innovation on 0 to 2pi maping

        Takes two scalars as input and returns np shape (1,1)
        '''

        # Predefines error
        e = np.zeros((1,1), dtype = np.float32)

        # wraps measurement
        yawMeasure = np.remainder(yawMeasure, 2*np.pi)

        # Computes the two possible solutions
        e1 = yawMeasure - yawPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e[0][0] = e1
        else:
            e[0][0] = e2

        return e

    def __innovationPhiTheta(self,angMeasure, angPredict):
        '''
        Function to return "geodesic" innovation on -pi to pi maping

        Takes two scalars as input and returns scalar
        '''

        # Wraps measurement
        angMeasure = self.__modPiToPi(angMeasure)

        # Predefines error
        e = 0.0

        e1 = angMeasure - angPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        return e

    def __computeKalman(self,H,R):
        '''
        Function to compute kalman gain

        Input       H np shape (n,m)
                    R np shape (n,n)

        Output      K np shape (m,m)
        '''

        S = H @ self.__P @ H.T + R

        return self.__P @ H.T @ np.linalg.inv(S)

    def __update(self,K,y,H):
        '''
        Function to update estimates

        Input       K np shape (m,n)
                    y np shape (n,1)
                    H np shape (m,n)
        '''


        # Updates estimate
        self.__x = self.__x + K @ y
        self.__P = (np.eye(self.__nStates, dtype=np.float32) - K @ H) @ self.__P

    def __angleLeveling(self, acc):
        '''
        Function to do leveling based on imu acceleration data

        Input   acc np shape (3,1)
        '''

        ## Calculating phi and theta based on atan2
        # phi
        phiMeasure = np.arctan2(-acc[1,0],-acc[2,0])
        
        # theta
        den = np.sqrt(acc[1,0]**2 + acc[2,0]**2)
        thetaMeasure = np.arctan2(acc[0,0], den)
        
        # gets predict value
        phiPredict = self.__x[0,0]
        thetaPredict = self.__x[1,0]

        # Covariance
        #R = self.__R_leveling
        devFromG = np.abs(np.linalg.norm(acc) - self.__gravity)
        rScalar = self.__R_leveling*(1.0 + 5000.0*(devFromG + devFromG**2))
        R = rScalar*np.eye(2, dtype = np.float32)
        R = R.astype(np.float32)
        ## Kalman stuff
        # Measurement H matrix
        H = self.__hLeveling

        # Innovation
        y = np.zeros((2,1), dtype = np.float32)
        y[0,0] = self.__innovationPhiTheta(phiMeasure, phiPredict)
        y[1,0] = self.__innovationPhiTheta(thetaMeasure, thetaPredict)
        
        # Kalman
        K = self.__computeKalman(H,R)

        # Update
        self.__update(K,y,H)

    def __yawMeasurement(self, yawMeasure, R):
        '''
        Function to do kalman routine on imu data

        Input       imuData np shape (6,1)
                    R       scalar
        '''
        
        ## Kalman routine
        # H matrix
        H = self.__hYaw

        # Defines yaw as np array with shape (1,1)

        # computing innovation
        yawPredict = self.__x[2,0]
        y = self.__innovationYaw(yawMeasure, yawPredict)
        
        # computes the Kalman gain
        K = self.__computeKalman(H,R)

        # Updates states
        self.__update(K,y,H)

    def __yawSet(self, yawMeasure):
        '''
        Function to set the yaw directly, this is to be used when there is a "large" jump in the pf solution

        input yawMeasure np shape (3,1)
        '''

        self.__x[2,0] = yawMeasure
        self.__P[2,2] = self.__PInit[2,2]

    def __yawEvaluation(self, yawMeasure, R):
        '''
        Function to decide if position is to be passed as a measurement or a direct replacement of the position in the filter

        If position from pf filter jumps the prediction of the pf filter is passed directly to the states to prevent a false velocity spike
        This velocity spike will then move the particles in a wrong direction, further worsening the problem

        Input       pose np shape (3,1) on form [x y z]'
                    R    np shape (3,3)
        '''

        # Gets predicted value
        yawPredict = self.__x[2,0]

        # calculates norm predicted to "measurement"
        norm = np.abs(self.__innovationYaw(yawMeasure, yawPredict))

        if norm <= self.__yawThreshold:
            self.__yawMeasurement(yawMeasure, R)
        else:
            self.__yawSet(yawMeasure)

    # Public set functions
    def setImuMeasurement(self, acc_s, omg_s, dt, online = True):
        '''
        Updates Imu measuremen related states
        
        Also executes leveling subroutine if acc is close to gravity

        input   acc np shape (3,1)
                omg np shape (3,1)
        '''
        ## Transforms data from sensor(s) frame to estimation(e) frame
        acc_e   = self.__rotMat_bs @ acc_s
        omg_e  = self.__rotMat_bs @ omg_s

        ## Leveling, only when there is low accelerations
        # Abs of sensor acceleration
        absAcc = np.linalg.norm(acc_e)

        # Acceleration thereshold
        epsilon = self.__accHighThreshold
        lowThreshold = self.__gravity*(1-epsilon)
        highThreshold = self.__gravity*(1+epsilon)

        if lowThreshold < absAcc and absAcc < highThreshold:
            self.__angleLeveling(acc_e)


        ## Predict stuff
        ## Gyro data
        self.__w = omg_e

        ## Calls predict function
        self.__predict(dt, online = online)

    def setYawMeasurementWithCovariance(self, yaw_n, R, online = True):
        '''
        Function to set and update yaw measurement

        input   yaw_n scalar
                R     scalar
        '''

        R = np.float32(R)

        # Sets measurement
        if online:
            self.__yawEvaluation(yaw_n, R)

    def setFilterParameters(self, params):
        '''
        Function used to set filter parameters, this function should be ran once afther creating the ins object
        (Alt, can be called in init function of this classes construction, and have param class as init parameter)
        '''

        # Measurement uncertainty covariance
        self.__R_yaw        = params.rYaw
        self.__R_leveling   = params.rLevel

        # Acc related
        self.__accHighThreshold = params.levelingWindow
        self.__gravity = params.gravity

        self.__yawThreshold = params.yawThreshold

        # State transition covariance matrix
        Q = np.eye(self.__nStates, dtype = np.float32)
        Q[0:3, 0:3] = params.qAngVel*np.eye(3, dtype = np.float32)
        Q[3:6, 3:6] = params.qBiasGyro*np.eye(3, dtype = np.float32)
        # Sets to self variable
        self.__Q = Q

        # Sensor to estimation frame transform
        self.__rotMat_bs = params.rotMat_bs

        # Initial condition
        # tHeta
        self.__x[0:3] = params.initState[12:15]
        # Bias
        self.__x[3:6] = params.initState[18:21]

        self.__xInit = self.__x

        # tHeta
        self.__P[0:3,0:3] = params.initCov[12:15,12:15]
        # Bias
        self.__P[3:6,3:6] = params.initCov[18:21,18:21]

        self.__PInit = self.__P

    # Public get functions
    def getOrientationWithCovariance(self):
        '''
        Function to get orientation vector tHeta_nb from body to ned

        Output:     tHeta_nb np shape (3,1)
                    rOri     np shape (3,3)
        '''

        # Gets tHeta from states

        return self.__x[0:3], self.__P[0:3,0:3]

    def getAngularVelocityWithCovariance(self):
        '''
        Function to get angular velocities in estimation frame

        Output:     omg_e_nb np shape (3,1)
                    (e (estimation) frame is typically aligned with body frame)
                    rOmg     np shape (3,3)
        '''
        
        return self.__w - self.__x[3:6], self.__P[3:6,3:6]

    def getGyroBiasWithCovariance(self):
        '''
        Function to get gyro bias
        '''

        return self.__x[3:6], self.__P[3:6, 3:6]
    
    # Jit init functions
    def resetFilter(self, keepAngles = False):
        '''
        Function to reset filter parameters after running jitInit Function
        '''

        ## System state vector
        # [  0,     1,   2,   3,  4,  5]
        # [phi, theta, psi,  pb, qb, rb]
        if keepAngles == True:
            self.__x[2,0] = self.__xInit[2,0]
            self.__x[5,0] = self.__xInit[5,0]
            self.__P[2,2] = self.__PInit[2,2]
            self.__P[5,5] = self.__PInit[5,5]
        else:
            self.__x = self.__xInit
            self.__P = self.__PInit
        

        print('warn: ros_node_ekf: ori filter reset')

    def __dryRun(self):
        '''
        Function to run all functions in class to get them jit compiled
        '''

        # Predict
        dummyDt = np.float32(0.0)
        self.predict()
        self.__predict(dummyDt)
        self.__controlInput(dummyDt)
        self.__covariancePredictWeight(dummyDt)
        # WrapEulerAngles
        self.__wrapEulerAngles()
        # ModPiToPi
        self.__modPiToPi(1.0)
        # State transition
        dummyDt = np.float32(0.0)
        self.__stateTransitionLin(dummyDt)
        self.__covariancePredictWeight(dummyDt)
        # Innovation lin
        dummyXMeasure = np.zeros((1,1), dtype = np.float32)
        dummyHmat = np.zeros((1,self.__nStates), dtype = np.float32)
        dummyHmat[0,0] = 1.0
        dummyY = self.__innovationLin(dummyXMeasure,dummyHmat)
        # InnovationYaw
        self.__innovationYaw(0.0, 0.0)
        # InnovationPi
        self.__innovationPhiTheta(0.0, 0.0)
        # KalmanCompute
        dummyR = np.eye(1, dtype = np.float32)
        dummyK = self.__computeKalman(dummyHmat, dummyR)
        # Update
        self.__update(dummyK, dummyY, dummyHmat)
        # AngleLeveling
        dummyAcc = np.zeros((3,1), dtype = np.float32)
        self.__angleLeveling(dummyAcc)
        # YawMeasure
        rYaw = np.float32(1.0)
        self.__yawMeasurement(0.0, rYaw)
        self.__yawSet(0.0)
        self.__yawEvaluation(0.0, rYaw)
        # SetImuMeasurement
        dummyGyro = np.zeros((3,1), dtype = np.float32)
        dummyTime = np.float32(0.0)
        self.setImuMeasurement(dummyAcc, dummyGyro, dummyTime)
        # SetYawMeasurement
        self.setYawMeasurementWithCovariance(0.0, rYaw)
        # SetFilterParameters
        dummyFilterParams = InsParameters()
        self.setFilterParameters(dummyFilterParams)
        # GetOrientation
        self.getOrientationWithCovariance()
        # GetAngularVelocity
        self.getAngularVelocityWithCovariance()

        self.resetFilter()

    def jitInit(self):
        '''
        Function to get all functions in class jit compiled
        '''

        self.__dryRun()
        self.resetFilter()


