
from llvmlite import binding
binding.set_option("tmp", "-non-global-value-max-name-size=8192")


# Ros imports
import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from rclpy.time import Time

from std_msgs.msg import Bool
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Vector3, TwistWithCovarianceStamped, PoseWithCovarianceStamped



# Configuration
from idl_botsy_pkg.softwareConfigutarion import *

# Filter Specifications
from idl_botsy_pkg.filterConfig import FilterInitialStates
from idl_botsy_pkg.filterConfig import FilterConfiguration
from idl_botsy_pkg.filterConfig import EkfRates

# Selecting what Imu msg definition to use
if pX4Sensor:
    from px4_msgs.msg import SensorCombined as Imu
else:
    from sensor_msgs.msg import Imu as Imu

# Selecting what filter tuning to load
if simulation == True:
    if pX4Sensor == False:
        from idl_botsy_pkg.filterConfig import ImuGazeboMain as InsImuSensorConfig
    else:
        from idl_botsy_pkg.filterConfig import ImuPX4SimMain as InsImuSensorConfig
else:
    from idl_botsy_pkg.filterConfig import ImuPX4RealMain as InsImuSensorConfig



# Selecting wether to use JIT or not (DO NOT! try to run real time without JIT)
from idl_botsy_pkg.filterConfig import kalmanFilterConfigurationJitCompile
from idl_botsy_pkg.filterConfig import kalmanFilterConfigurationSplit

if kalmanFilterConfigurationJitCompile == True:
    if kalmanFilterConfigurationSplit == True:
        from idl_orientation_pkg.JITextendedKalmanFilterSplit import InsEkf
    else:
        from idl_orientation_pkg.JITextendedKalmanFilterCombined import InsEkf
    from idl_orientation_pkg.JITextendedKalmanFilterParameters import InsParameters
    from idl_botsy_pkg.JITdroneConfiguration import DroneGeometry
else:
    if kalmanFilterConfigurationSplit == True:
        from idl_orientation_pkg.extendedKalmanFilterSplit import InsEkf
    else:
        from idl_orientation_pkg.extendedKalmanFilterCombined import InsEkf
    from idl_orientation_pkg.extendedKalmanFilterParameters import InsParameters
    from idl_botsy_pkg.droneConfiguration import DroneGeometry


# Math stuff
from scipy.spatial.transform import Rotation as R
import numpy as np




class EkfOrientationNode(Node):

    def __init__(self):
        super().__init__('ekfOrientationNode')

        ## Setting up INS params
        droneGeom = DroneGeometry()
        insMainParam = InsParameters()
        initStates = FilterInitialStates()
        insConfig = FilterConfiguration()
        imuSensor = InsImuSensorConfig()
        insRates = EkfRates()

        # System state predict uncertainty matrix
        insMainParam.qPosition   = imuSensor.qPosition
        insMainParam.qAngles     = imuSensor.qAngles
        insMainParam.qLinVel     = imuSensor.qLinVel
        insMainParam.qAngVel     = imuSensor.qAngVel
        insMainParam.qLinAcc     = imuSensor.qLinAcc
        insMainParam.qAngAcc     = imuSensor.qAngAcc
        insMainParam.qBiasAcc    = imuSensor.qBiasAcc
        insMainParam.qBiasGyro   = imuSensor.qBiasGyro
        insMainParam.qGravity    = imuSensor.qGravity

        # Measurement uncertainties covariance
        insMainParam.rYaw   = imuSensor.rYaw
        insMainParam.rPos   = imuSensor.rPos
        insMainParam.rAcc   = imuSensor.rAcc
        insMainParam.rGyro  = imuSensor.rGyro
        insMainParam.rLevel = imuSensor.rLevel

        # Time delay for pose msg for filter to go offline
        insMainParam.timeMaxDelayPose = imuSensor.timeMaxDelayPose

        # FilterConfig
        insMainParam.deltaImuCum        = insConfig.deltaImuCum
        insMainParam.secondOrderPredict = insConfig.secondOrderPredict
        insMainParam.fixedRatePredict   = insConfig.fixedRatePredict

        # Acc related
        insMainParam.levelingWindow = imuSensor.levelingWindow
        
        # Pos related
        insMainParam.posThreshold = imuSensor.posThreshold
        insMainParam.yawThreshold = imuSensor.yawThreshold

        # Geometry
        insMainParam.rotMat_bs = imuSensor.rotMat_bs.astype(np.float32)
        insMainParam.pos_b_bs = imuSensor.pos_b_bs.astype(np.float32)

        ## Initial state
        # Geting states from global definition and populating kalman filter specific definition class

        # Pos related
        insMainParam.initState[0:3] = initStates.pos.asNpArray()
        insMainParam.initState[3:6] = initStates.linVel.asNpArray()
        insMainParam.initState[6:9] = initStates.linAcc.asNpArray()
        insMainParam.initState[9:12] = initStates.accBias.asNpArray()
        # Angle related
        insMainParam.initState[12:15] = initStates.tHeta.asNpArray()
        insMainParam.initState[15:18] = initStates.angVel.asNpArray()
        insMainParam.initState[18:21] = initStates.omgBias.asNpArray()

        ## Initial cov
        # Pos related
        insMainParam.initCovVec[0:3] = initStates.posCov.asNpArray()
        insMainParam.initCovVec[3:6] = initStates.linVelCov.asNpArray()
        insMainParam.initCovVec[6:9] = initStates.linAccCov.asNpArray()
        insMainParam.initCovVec[9:12] = initStates.accBiasCov.asNpArray()
        # Angle related
        insMainParam.initCovVec[12:15] = initStates.tHetaCov.asNpArray()
        insMainParam.initCovVec[15:18] = initStates.angVelCov.asNpArray()
        insMainParam.initCovVec[18:21] = initStates.omgBiasCov.asNpArray()
        

        ## Selecting position msg source for the kalman filter
        if insConfig.gazeboGT:
            posMsgSource = 'gazeboGT/'
        else:
            posMsgSource = 'pf/'

        ## Selecting imu topic to subscribe to
        if pX4Sensor:
            imuMsgSource = '/SensorCombined_PubSubTopic'
            imuMsgQosProfile = qos_profile_sensor_data
        else:
            imuMsgSource = 'sensor/imu_main'
            imuMsgQosProfile = qos_profile_sensor_data


        ## Publish body rates
        self.__pubBodyVel = False

        ## INS Main object
        # Predict rate
        ekfInsPredictHz = insRates.ekfInsPredictHz
        ekfInsPredictDt = 1/ekfInsPredictHz

        # Creating object
        self.__insMain = InsEkf(ekfInsPredictDt)
        self.get_logger().info('Ins Object Created')
        
        # Calls member function to run all member functions in classes to ge them jit compiled
        self.get_logger().info('Ins Object Jit Compilation Started')
        self.__insMain.jitInit()
        self.get_logger().info('Ins Object Jit Compilation Complete')

        # Sets filter parameters
        self.__insMain.setFilterParameters(insMainParam)
        self.get_logger().info('Ins Parameters set')
        self.get_logger().info('Starting Node work!')


        ## Timers

        # EKF predict timer
        self.__ekfPredictTimer = self.create_timer( ekfInsPredictDt,
                                                    self.__insMainPredictCallback)

        # Service routine timer for ins main
        ekfServiceHz = insRates.ekfServiceHz
        ekfServiceDt = 1.0/ekfServiceHz
        self.__ekfServiceRoutineTimer = self.create_timer(  ekfServiceDt,
                                                            self.__insMainCheckTimers)

        ## Publish timers
        # Velocity body publish timer
        ekfVelBodyPubHz = insRates.ekfVelBodyPubHz
        ekfVelBodyPubDt = 1.0/ekfVelBodyPubHz
        if self.__pubBodyVel == True:
            self.__ekfVelBodyPubTimer = self.create_timer(  ekfVelBodyPubDt,
                                                            self.__insPublishVelBodyTimerCallback)

        # Velocity level publish timer
        ekfVelLevelPubHz = insRates.ekfVelLevelPubHz
        ekfVelLevelPubDt = 1.0/ekfVelLevelPubHz
        self.__ekfVelLevelPubTimer = self.create_timer( ekfVelLevelPubDt,
                                                        self.__insPublishVelLevelTimerCallback)

        # Pose publish timer
        ekfPosNedPubHz = insRates.ekfPosNedPubHz
        ekfPosNedPubDt = 1.0/ekfPosNedPubHz
        self.__ekfPosNedPubTimer = self.create_timer(   ekfPosNedPubDt,
                                                        self.__insPublishPosNedTimerCallback)

        # Odom publish timer
        ekfOdomNedPubHz = insRates.ekfOdomNedPubHz
        ekfOdomNedPubDt = 1.0/ekfOdomNedPubHz
        self.__ekfPosNedPubTimer = self.create_timer(   ekfOdomNedPubDt,
                                                        self.__insPublishOdomNedTimerCallback)

        # SensorBias publish timer
        ekfSensorBiasPubHz = insRates.ekfSensorBiasPubHz
        ekfSensorBiasPubDt = 1.0/ekfSensorBiasPubHz
        self.__ekfAccBiasPubTimer = self.create_timer(  ekfSensorBiasPubDt,
                                                        self.__insPublishSensorBiasTimerCallback)


        ## Subscribers
        # Main imu subscriber
        self.__ekfImuSubscriber = self.create_subscription( Imu,
                                                            imuMsgSource,
                                                            self.__insMainImuMeasurementCallback,
                                                            imuMsgQosProfile)

        # Pose subscription
        self.__ekfPoseSubscriber = self.create_subscription(PoseWithCovarianceStamped, 
                                                            posMsgSource + 'pose_ned',
                                                            self.__insMainPoseMeasurementCallback,
                                                            10)

        # Pose subscription
        self.__insResetSubscriber = self.create_subscription(   Bool, 
                                                                'ins/system/reset',
                                                                self.__insResetStatesCallback,
                                                                10)

        ## Publisher
        # Velocity body publisher
        self.__ekfVelBodyPublisher = self.create_publisher( TwistWithCovarianceStamped,
                                                            'ekf/vel_body',
                                                            10)

        # Velocity level publisher
        self.__ekfVelLevelPublisher = self.create_publisher( TwistWithCovarianceStamped,
                                                            'ekf/vel_level',
                                                            10)

        # Position publisher
        self.__ekfPosNedPublisher = self.create_publisher(  PoseWithCovarianceStamped,
                                                            'ekf/pose_ned',
                                                            10)

        # Position publisher
        self.__ekfOdomNedPublisher = self.create_publisher( Odometry,
                                                            'botsy/odom/body',
                                                            qos_profile_sensor_data)

        # Position publisher
        self.__insStatePublisher = self.create_publisher(   Bool,
                                                            'ins/system/ekf_online',
                                                            10)

        # Accelerometer bias publisher
        self.__insSensorBiasPublisher = self.create_publisher(  TwistWithCovarianceStamped,
                                                                'ekf/sensor_bias',
                                                                10)

    ## Timer callback functions
    # Predict
    def __insMainPredictCallback(self):
        '''
        Function to run predict on timer callback
        '''
        # Predicting state
        self.__insMain.predict()

    # Service routine for ins filter
    def __insMainCheckTimers(self):
        '''
        Function to check elapse of timers in ins filter object
        '''

        # Checking timings in filter
        timeOfCall = self.get_clock().now().to_msg()
        timeNsec = Time.from_msg(timeOfCall).nanoseconds
        timeSec = timeNsec*10**(-9)

        self.__insMain.checkTiming(timeSec)


    ## subscriber callbacks

    # Pose
    def __insMainPoseMeasurementCallback(self, msg):
        '''
        Function to set insMain position from pf
        '''

        # Gets position
        pos_n_nb = msg.pose.pose.position

        # Gets Theta angle
        theta = msg.pose.pose.orientation.z

        # Stitching together to a pose vector
        pose = np.array([[pos_n_nb.x],[pos_n_nb.y],[pos_n_nb.z],[theta]], dtype = np.float32)

        # Gets covariance
        varFromMsg = msg.pose.covariance.reshape(6,6)

        # Formats to "filter from"
        rPose = np.zeros((4,4), dtype = np.float32)
        # Position related
        rPose[0:3, 0:3] = varFromMsg[0:3, 0:3]
        # Theta
        rPose[3, 3] = varFromMsg[5, 5]

        # gets time
        timeNsec = Time.from_msg(msg.header.stamp).nanoseconds
        timeSec = timeNsec*10**(-9)

        self.__insMain.setPoseMeasurementWithCovariance(pose, rPose, timeSec)

    # main imu
    if pX4Sensor:
        def __insMainImuMeasurementCallback(self, msg):
            '''
            Function to set insMain imu data form sensor imu
            '''

            imuData, timeSec = self.__sensorCombinedMsg2VecAndTime(msg)
            
            # Sending to ins system
            
            self.__insMain.setImuMeasurement(imuData, timeSec)
    else:
        def __insMainImuMeasurementCallback(self, msg):
            '''
            Function to set insMain imu data form sensor imu
            '''

            imuData, timeSec = self.__imuMsg2vecAndTime(msg)
            
            # Sending to ins system
            
            self.__insMain.setImuMeasurement(imuData, timeSec)

    # main imu IMU mag unpacker function
    def __imuMsg2vecAndTime(self, msg):
        '''
        Function to unpack gazbo/ros Imu msg data to vector and time format
        '''
        ## Parsing data
        # Gets sensor data
        acc = msg.linear_acceleration
        omg = msg.angular_velocity

        # gets time
        timeOfCall = self.get_clock().now().to_msg()
        timeNsec = Time.from_msg(timeOfCall).nanoseconds
        timeSec = timeNsec*10**(-9)
        

        # Populating np array
        imuData = np.array([[acc.x],[acc.y],[acc.z],[omg.x],[omg.y],[omg.z]], dtype = np.float32)

        return imuData, timeSec

    def __sensorCombinedMsg2VecAndTime(self, msg):
        '''
        Function to unpack PX4 sensorCombined msg data to vector and time format
        '''

        ## Parsing data
        # Gets sensor data
        acc = msg.accelerometer_m_s2
        omg = msg.gyro_rad
        
        # Gets time
        timeMsec = msg.timestamp
        timeSec = timeMsec*10**(-6)

        imuData = np.array([[acc[0]],[acc[1]],[acc[2]],[omg[0]],[omg[1]],[omg[2]]], dtype = np.float32)

        return imuData, timeSec

    # Ins state reset
    def __insResetStatesCallback(self, msg):
        '''
        Function to reset states in kalman filter
        '''

        if msg.data:
            self.__insMain.resetFilter()
            self.get_logger().info('Filter reset')


    ## Publish timer callbacks
    # Vel body
    def __insPublishVelBodyTimerCallback(self):
        '''
        Function to publish velocity msg on timer callback
        '''
        
        # Gets linear velocity and associated covariance
        if self.__insMain.getPosFilterOnlineState():
            vel_b_bn, rVel_b_bn = self.__insMain.getLinearVelocityBodyWithCovariance()
        else:
            vel_b_bn = np.zeros((3,1), dtype = np.float32)
            rVel_b_bn = np.zeros((3,3), dtype = np.float32)

        # Gets linear velocity and associated covariance
        omg_b_bn, rOmg_b_bn = self.__insMain.getAngularVelocityBodyWithCovariance()

        # Creates vel and omg for msg
        velForMsg_b_bn = vel_b_bn.astype(np.float64)
        omgForMsg_b_bn = omg_b_bn.astype(np.float64)

        # Creates covariance vector for msg format
        cov = np.zeros((6,6), dtype = np.float64)
        cov[0:3, 0:3] = rVel_b_bn
        cov[3:6, 3:6] = rOmg_b_bn
        covVec = cov.reshape(36).astype(np.float64)

        # Creating empty msg to populate
        msg = TwistWithCovarianceStamped()

        # Populating msg
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'body'

        msg.twist.covariance = covVec

        msg.twist.twist.linear.x = velForMsg_b_bn[0,0]
        msg.twist.twist.linear.y = velForMsg_b_bn[1,0]
        msg.twist.twist.linear.z = velForMsg_b_bn[2,0]

        msg.twist.twist.angular.x = omgForMsg_b_bn[0,0]
        msg.twist.twist.angular.y = omgForMsg_b_bn[1,0]
        msg.twist.twist.angular.z = omgForMsg_b_bn[2,0]

        # Publishes msg
        self.__ekfVelBodyPublisher.publish(msg)

    # Vel level
    def __insPublishVelLevelTimerCallback(self):
        '''
        Function to publish velocity msg on timer callback
        '''
        
        # Gets linear velocity and associated covariance
        if self.__insMain.getPosFilterOnlineState():
            vel_l_bn, rVel_l_bn = self.__insMain.getLinearVelocityLevelWithCovariance()
        else:
            vel_l_bn = np.zeros((3,1), dtype = np.float32)
            rVel_l_bn = np.zeros((3,3), dtype = np.float32)

        # Gets linear velocity and associated covariance
        omg_l_bn, rOmg_l_bn = self.__insMain.getAngularVelocityLevelWithCovariance()

        # Creates vel and omg for msg
        velForMsg_l_bn = vel_l_bn.astype(np.float64)
        omgForMsg_l_bn = omg_l_bn.astype(np.float64)

        # Creates covariance vector for msg format
        cov = np.zeros((6,6), dtype = np.float64)
        cov[0:3, 0:3] = rVel_l_bn
        cov[3:6, 3:6] = rOmg_l_bn
        covVec = cov.reshape(36).astype(np.float64)

        # Creating empty msg to populate
        msg = TwistWithCovarianceStamped()

        # Populating msg
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'level'

        msg.twist.covariance = covVec

        msg.twist.twist.linear.x = velForMsg_l_bn[0,0]
        msg.twist.twist.linear.y = velForMsg_l_bn[1,0]
        msg.twist.twist.linear.z = velForMsg_l_bn[2,0]

        msg.twist.twist.angular.x = omgForMsg_l_bn[0,0]
        msg.twist.twist.angular.y = omgForMsg_l_bn[1,0]
        msg.twist.twist.angular.z = omgForMsg_l_bn[2,0]

        # Publishes msg
        self.__ekfVelLevelPublisher.publish(msg)

    # Pose ned
    def __insPublishPosNedTimerCallback(self):
        '''
        Function to publish velocity msg on timer callback
        '''
        
        # Gets position and associated covariance
        if self.__insMain.getPosFilterOnlineState():
            pos_n_bn, rPos_n_bn = self.__insMain.getPositionWithCovariance()
        else:
            pos_n_bn = np.zeros((3,1), dtype = np.float32)
            rPos_n_bn = np.zeros((3,3), dtype = np.float32)

        # Gets orientation and associated covariance
        tHeta_nb, rTHeta_nb = self.__insMain.getOrientationWithCovariance()

        # Creates vel and omg for msg
        posForMsg_n_bn = pos_n_bn.astype(np.float64)
        tHetaForMsg_nb = tHeta_nb.astype(np.float64)

        # Creates covariance vector for msg format
        cov = np.zeros((6,6), dtype = np.float64)
        cov[0:3, 0:3] = rPos_n_bn
        cov[3:6, 3:6] = rTHeta_nb
        covVec = cov.reshape(36).astype(np.float64)

        # Creating empty msg to populate
        msg = PoseWithCovarianceStamped()

        # Populating msg
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'ned'

        msg.pose.covariance = covVec

        msg.pose.pose.position.x = posForMsg_n_bn[0,0]
        msg.pose.pose.position.y = posForMsg_n_bn[1,0]
        msg.pose.pose.position.z = posForMsg_n_bn[2,0]

        msg.pose.pose.orientation.x = tHetaForMsg_nb[0,0]
        msg.pose.pose.orientation.y = tHetaForMsg_nb[1,0]
        msg.pose.pose.orientation.z = tHetaForMsg_nb[2,0]
        if self.__insMain.getPosFilterOnlineState():
            msg.pose.pose.orientation.w = -2.0
        else:
            msg.pose.pose.orientation.w = -3.0

        # Publishes msg
        self.__ekfPosNedPublisher.publish(msg)

    # Odom ned
    def __insPublishOdomNedTimerCallback(self):
        '''
        Function to publish location to odom msg to tf listener node
        '''

        # Cerates a msg and populates
        msg = Odometry()

        msg.header.stamp = self.get_clock().now().to_msg()


        # Populating Pose part of msg
        # Gets position and associated covariance
        if self.__insMain.getPosFilterOnlineState():
            pos_n_bn, rPos_n_bn = self.__insMain.getPositionWithCovariance()
        else:
            pos_n_bn = np.zeros((3,1), dtype = np.float32)
            rPos_n_bn = np.zeros((3,3), dtype = np.float32)

        # Gets orientation and associated covariance
        tHeta_nb, rTHeta_nb = self.__insMain.getOrientationWithCovariance()
        tHeta_nb = np.array([tHeta_nb[2,0], tHeta_nb[1,0], tHeta_nb[0,0]])
        rotObj = R.from_euler('ZYX', tHeta_nb.reshape(3), degrees= False)
        quat_nb = rotObj.as_quat()

        # Creates vel and omg for msg
        posForMsg_n_bn = pos_n_bn.astype(np.float64)

        # Creates covariance vector for msg format
        cov = np.zeros((6,6), dtype = np.float64)
        cov[0:3, 0:3] = rPos_n_bn
        cov[3:6, 3:6] = rTHeta_nb
        covVec = cov.reshape(36).astype(np.float64)

        msg.pose.covariance = covVec

        msg.pose.pose.position.x = posForMsg_n_bn[0,0]
        msg.pose.pose.position.y = posForMsg_n_bn[1,0]
        msg.pose.pose.position.z = posForMsg_n_bn[2,0]

        msg.pose.pose.orientation.x = quat_nb[0]
        msg.pose.pose.orientation.y = quat_nb[1]
        msg.pose.pose.orientation.z = quat_nb[2]
        msg.pose.pose.orientation.w = quat_nb[3]

        '''
        # Populating Twist Part of msg
        # Gets linear velocity and associated covariance
        vel_b_bn, rVel_b_bn = self.__insMain.getLinearVelocityBodyWithCovariance()

        # Gets linear velocity and associated covariance
        omg_b_bn, rOmg_b_bn = self.__insMain.getAngularVelocityBodyWithCovariance()

        # Creates vel and omg for msg
        velForMsg_b_bn = vel_b_bn.astype(np.float64)
        omgForMsg_b_bn = omg_b_bn.astype(np.float64)

        # Creates covariance vector for msg format
        cov = np.zeros((6,6), dtype = np.float64)
        cov[0:3, 0:3] = rVel_b_bn
        cov[3:6, 3:6] = rOmg_b_bn
        covVec = cov.reshape(36).astype(np.float64)

        # Populating msg
        msg.twist.covariance = covVec

        msg.twist.twist.linear.x = velForMsg_b_bn[0,0]
        msg.twist.twist.linear.y = velForMsg_b_bn[1,0]
        msg.twist.twist.linear.z = velForMsg_b_bn[2,0]

        msg.twist.twist.angular.x = omgForMsg_b_bn[0,0]
        msg.twist.twist.angular.y = omgForMsg_b_bn[1,0]
        msg.twist.twist.angular.z = omgForMsg_b_bn[2,0]
        '''

        # Publishes msg
        self.__ekfOdomNedPublisher.publish(msg)

    # Acc bias
    def __insPublishSensorBiasTimerCallback(self):
        '''
        Function to publish acc bias
        '''

        # Gets linear velocity and associated covariance
        if self.__insMain.getPosFilterOnlineState():
            accBias, rAccBias = self.__insMain.getAccBiasWithCovariance()
        else:
            accBias = np.zeros((3,1), dtype = np.float32)
            rAccBias = np.zeros((3,3), dtype = np.float32)

        # Gets linear velocity and associated covariance
        gyroBias, rGyroBias = self.__insMain.getGyroBiasWithCovariance()

        # Creates vel and omg for msg
        accBiasMsg = accBias.astype(np.float64)
        gyroBiasMsg = gyroBias.astype(np.float64)

        # Creates covariance vector for msg format
        cov = np.zeros((6,6), dtype = np.float64)
        cov[0:3, 0:3] = rAccBias
        cov[3:6, 3:6] = rGyroBias
        covVec = cov.reshape(36).astype(np.float64)

        # Creating empty msg to populate
        msg = TwistWithCovarianceStamped()

        # Populating msg
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'bias'

        msg.twist.covariance = covVec

        msg.twist.twist.linear.x = accBiasMsg[0,0]
        msg.twist.twist.linear.y = accBiasMsg[1,0]
        msg.twist.twist.linear.z = accBiasMsg[2,0]

        msg.twist.twist.angular.x = gyroBiasMsg[0,0]
        msg.twist.twist.angular.y = gyroBiasMsg[1,0]
        msg.twist.twist.angular.z = gyroBiasMsg[2,0]

        # Publishing msg
        self.__insSensorBiasPublisher.publish(msg)



def main(args=None):
    rclpy.init(args=args)

    # Creates INS node
    insSystem = EkfOrientationNode()

    # Spins node to keep it alive
    rclpy.spin(insSystem)

    # Destroys node on shutdown
    insSystem.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()



