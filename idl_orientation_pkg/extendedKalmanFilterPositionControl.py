

import numpy as np


# Import Drone config class
from idl_botsy_pkg.droneConfiguration import DroneGeometry
from idl_orientation_pkg.extendedKalmanFilterParameters import InsParameters






# Position EKF
class InsPosition(object):

    # Dt as input parameter is used to keep it in the same interface as other implementations
    def __init__(self, dt):

        ## System Number of states
        self.__nStates = 9

        ## Filter settings

        # Filter state
        self.online = True

        # Initialization of filter
        self.__stateInitialization = 0

        # Position threshold
        self.__posThreshold = 1.0

        ## System state vector
        # [0, 1, 2, 3, 4, 5,   6,   7,   8]
        # [x, y, z, u, v, w, abx, aby, abz]
        self.__x = np.zeros((self.__nStates,1), dtype=np.float32)
        self.__xInit = self.__x

        # Acceleration
        self.__a = np.zeros((3,1), dtype = np.float32)

        ## Orientation
        self.__tHeta = np.zeros((3,1), dtype = np.float32)
        
        ## Predict and state covariance
        # System state covariance matrix
        self.__P = np.eye(self.__nStates, dtype=np.float32)
        self.__PInit = self.__P

        ## System state predict uncertainty matrix
        self.__Q = np.eye(self.__nStates, dtype=np.float32)
        
        ## Measurement uncertainties covariance

        # Matrixes
        self.__R_pos        = np.eye(3, dtype=np.float32)

        # Measure functions
        I = np.eye(3, dtype = np.float32)
        self.__hPosition = np.zeros((3,self.__nStates), dtype = np.float32)
        self.__hPosition[0:3, 0:3] = I

        ## Geometry and transforms
        # Creates a Drone geometry object
        self.__droneGeom = DroneGeometry()

        # Transform from body to ned, this is computed and written to self each predict step
        self.__rotMat_bn = np.eye(3, dtype = np.float32)

        # Transform from sensor to estimation frame (body/Ned typically)
        self.__rotMat_bs = np.eye(3, dtype = np.float32)
        self.__orientationCovDet = np.float32(1.0)

        self.__pos_b_bs = np.zeros((3,1), dtype = np.float32)

        # Gravity
        self.__gravity = np.float32(9.81)
        self.__gVec = np.array([[np.float32(0.0)],[np.float32(0.0)],[-self.__gravity]], dtype = np.float32)

    def predict(self):
        '''
            Dummy Predict to keep the interface lords happy
        '''
        pass

    def __predict(self, dt):
        '''
            Used to predict the next filter state

            Input   dt is a scalar
        '''

        # Calculates curret linearization of the state transition equation
        F = self.__stateTransitionLin(dt, bias= True)

        B = self.__controlInput(dt)

        W = self.__covariancePredictWeight(dt)

        # Predicts state
        self.__x = F @ self.__x + B @ self.__a

        # Predicts state covariance matrix
        self.__P = F @ self.__P @ F.T + W @ self.__Q @ W.T

    def __stateTransitionLin(self, dt, bias= False):
        '''
            Function to return state transition matrix

            Returns
                np shape (9, 9)
        '''

        # Identity matrix
        I = np.eye(3, dtype = np.float32)

        ## State transition
        # Setsup state transition matrix
        F = np.eye(self.__nStates, dtype=np.float32)
        
        # Position related
        F[0:3, 3:6] = I*dt
        
        # Linear velocity related
        if bias:
            F[3:6,6:9] = -self.__rotMat_bn*dt

        return F

    def __controlInput(self, dt):
        '''
            Function to return control input matrix

            Returns
                np shape (9, 3)
        '''

        # Predefining B matrix
        B = np.zeros((9,3), dtype = np.float32)

        # Populates B matrix
        B[3:6,0:3] = self.__rotMat_bn * dt

        return B

    def __covariancePredictWeight(self, dt):
        '''
            Function to return covariance weight update

            return W np shape (9,9)
        '''

        # Creates eye mat
        W = np.eye(self.__nStates, dtype = np.float32)
        # Sets nonlinear therm
        W[3:6,3:6] = self.__rotMat_bn

        # Weights by dt
        W = W*dt

        return W

    def __innovationLin(self, xMeasure, H):
        '''
        Finds the error between the measurement and the predicted value, given a linear measurement function

        Input is    X_measure np shape (m,1)
                    H np shape (m,nStates)

        Output is   Y np shape (m,1)
        '''
        
        return xMeasure - H @ self.__x

    def __computeKalman(self,H,R):
        '''
        Function to compute kalman gain

        Input       H np shape (n,m)
                    R np shape (n,n)

        Output      K np shape (m,m)
        '''

        S = H @ self.__P @ H.T + R

        return self.__P @ H.T @ np.linalg.inv(S)

    def __update(self,K,y,H):
        '''
        Function to update estimates

        Input       K np shape (m,n)
                    y np shape (n,1)
                    H np shape (m,n)
        '''


        # Updates estimate
        self.__x = self.__x + K @ y
        self.__P = (np.eye(self.__nStates, dtype=np.float32) - K @ H) @ self.__P

    def __posMeasurement(self, posMeasure, R):
        '''
        Function to set pose measurement

        Input       pose np shape (3,1) on form [x, y, z]'
        '''

        # Gets H matrices and populates a bigger matrix
        H = self.__hPosition

        ## Kalman routine

        # Innovation
        y = self.__innovationLin(posMeasure, H)

        # Kalman gain
        K = self.__computeKalman(H,R)

        # Updates
        self.__update(K,y,H)

    def __posSet(self, posMeasure):
        '''
        Function to set the position directly, this is to be used when there is a "large" jump in the pf solution

        input posMeasure np shape (3,1)
        '''

        self.__x[0:3] = posMeasure
        self.__x[3:6] = np.zeros((3,1), dtype = np.float32)
        self.__P[0:6, 0:6] = self.__PInit[0:6, 0:6]

    def __posEvaluation(self, posMeasure, R):
        '''
        Function to decide if position is to be passed as a measurement or a direct replacement of the position in the filter

        If position from pf filter jumps the prediction of the pf filter is passed directly to the states to prevent a false velocity spike
        This velocity spike will then move the particles in a wrong direction, further worsening the problem

        Input       pose np shape (3,1) on form [x y z]'
                    R    np shape (3,3)
        '''

        # Gets predicted value
        posPredict = self.__x[0:3]

        # calculates norm predicted to "measurement"
        norm = np.linalg.norm(posMeasure-posPredict)

        if norm <= self.__posThreshold:
            self.__posMeasurement(posMeasure, R)
        else:
            self.__posSet(posMeasure)
            print('PoseSet')

    # Public set functions
    def setAccMeasurement(self, accMeasure, dt):
        '''
        Sets acc measurement to self state ready for next predict

        input   imuData np shape (3,1) with elements [ax ay az]'
                time is scalar and is time of the measurement
        '''

        ## Transforms data from sensor(s) frame to estimation(e) frame
        accMeasure_e = self.__rotMat_bs @ accMeasure

        ## Sets acc measurement to self variable
        self.__a = accMeasure_e - self.__rotMat_bn.T @ self.__gVec

        ## Calls predict function
        if self.online:
            self.__predict(dt)
       
    def setPosMeasurementWithCovariance(self, pos_n_ns, R):
        '''
        Function to set pose measurement in map frame

        Input       pose np shape (3,1) on form [x y z]'
                    R    np shape (3,3)
        '''

        # Casting covariance to float
        R = R.astype(np.float32)

        if self.online:
            self.__posEvaluation(pos_n_ns, R)

    def setOrientation(self, tHeta, tHetaCov):
        '''
        Function to set orientation of imu

        Input tHeta with np shape (3,1)
        '''

        ## Transforms
        # Rotation from body to global
        self.__rotMat_bn = self.__droneGeom.rotFun_bn(tHeta)

        # Calculating determinant of covariance matrix
        self.__orientationCovDet = np.linalg.det(tHetaCov)

    def setFilterParameters(self, params):
        '''
        Function used to set filter parameters, this function should be ran once afther creating the ins object
        (Alt, can be called in init function of this classes construction, and have param class as init parameter)
        '''

        # Measurement uncertainty covariance
        self.__R_pos        = params.rPos*np.eye(3, dtype=np.float32)

        # Gravity
        self.__gravity = params.gravity
        self.__gVec = np.array([[np.float32(0.0)],[np.float32(0.0)],[-self.__gravity]], dtype = np.float32)

        # Pos threshold
        self.__posThreshold = params.posThreshold

        # State transition covariance matrix
        Q = np.eye(self.__nStates, dtype = np.float32)
        Q[0:3,0:3]      = params.qPosition*np.eye(3, dtype = np.float32)
        Q[3:6,3:6]      = params.qLinVel*np.eye(3, dtype = np.float32)
        Q[6:9,6:9]      = params.qBiasAcc*np.eye(3, dtype = np.float32)

        # Sets to self variable
        self.__Q = Q

        # Sensor to estimation frame transform
        self.__rotMat_bs = params.rotMat_bs
        self.__pos_b_bs = params.pos_b_bs

        # Initial condition
        # Postion and velocity
        self.__x[0:6]   = params.initState[0:6]
        # Bias
        self.__x[6:9]   = params.initState[9:12]

        self.__xInit = self.__x

        # Postion and velocity
        self.__P[0:6,0:6] = params.initCov[0:6,0:6]
        # Bias
        self.__P[6:9,6:9] = params.initCov[9:12,9:12]
        
        self.__PInit = self.__P

    # Public get functions
    def getPositionWithCovariance(self):
        '''
        Function to get the estimated pose in ned frame

        Output:     pos_n_nb np shape (3,1)
                    rVel     np shape (3,3)
        '''

        # Returns in map frame
        return self.__x[0:3].copy(), self.__P[0:3,0:3]

    def getLinearVelocityWithCovariance(self):
        '''
        Function to get linear velocity in ned frame

        Output:     vel_n_ns np shape (3,1)
                    rVel     np shape (3,3)
        '''


        return self.__x[3:6], self.__P[3:6,3:6]

    def getLinearVelocityBodyWithCovariance(self, covCal = False):
        '''
        Function to get linear velocity in body frame

        Output:     vel_b_ns np shape (3,1)
                    rVel     np shape (3,3)
        '''

        vel_n_ns = self.__x[3:6]

        # Transforms to body
        rotMat_nb = self.__rotMat_bn.T
        vel_b_ns = rotMat_nb @ vel_n_ns

        # Transforms covariance
        rVel_n = self.__P[3:6,3:6]
        
        if covCal == True:
            rVel_b = rotMat_nb @ rVel_n @ rotMat_nb.T
        else:
            rVel_b = rVel_n

        return vel_b_ns, rVel_b

    def getAccBiasWithCovariance(self):
        '''
        Function to get acc bias

        Output with np shape (3,1)
        '''
        

        return self.__x[6:9], self.__P[6:9, 6:9]

    # Jit init functions
    def resetFilter(self):
        '''
        Function to reset filter parameters after running jitInit Function
        '''

        ## System state vector
        # [0, 1, 2, 3, 4, 5,  6,  7,  8,   9,  10,  11]
        # [x, y, z, u, v, w, ud, vd, wd, abx, aby, abz]
        self.__x = self.__xInit

        ## Orientation
        self.__tHeta = np.zeros((3,1), dtype = np.float32)

        ## Predict and state covariance
        # System state covariance matrix
        self.__P = self.__PInit

        print('warn: ros_node_ekf: pos filter reset')

    def __dryRun(self):
        '''
        Function to run all functions in class to get them jit compiled
        '''

        # Predict
        self.predict()
        # State transition
        dummyDt = np.float32(0.0)
        self.__predict(dummyDt)
        self.__stateTransitionLin(dummyDt)
        self.__controlInput(dummyDt)
        self.__covariancePredictWeight(dummyDt)
        # Innovation lin
        dummyXMeasure = np.zeros((1,1), dtype = np.float32)
        dummyHmat = np.zeros((1,self.__nStates), dtype = np.float32)
        dummyHmat[0,0] = 1.0
        dummyY = self.__innovationLin(dummyXMeasure,dummyHmat)
        # KalmanCompute
        dummyR = np.eye(1, dtype = np.float32)
        dummyK = self.__computeKalman(dummyHmat, dummyR)
        # Update
        self.__update(dummyK, dummyY, dummyHmat)
        # PosMeasure
        dummyPos = np.zeros((3,1), dtype = np.float32)
        dummyR = np.eye(3, dtype = np.float32)
        self.__posMeasurement(dummyPos, dummyR)
        self.__posSet(dummyPos)
        self.__posEvaluation(dummyPos, dummyR)
        # SetAccMeasure
        dummyAcc = np.zeros((3,1), dtype = np.float32)
        dummyTime = np.float32(0.0)
        self.setAccMeasurement(dummyAcc, dummyTime)
        # SetPosMeasure
        self.setPosMeasurementWithCovariance(dummyPos, dummyR)
        # SetOrientation
        dummyTheta = np.zeros((3,1), dtype = np.float32)
        dummyCov = np.ones((3,3), dtype = np.float32)
        self.setOrientation(dummyTheta, dummyCov)
        # SetFilterParams
        dummyFilterParams = InsParameters()
        self.setFilterParameters(dummyFilterParams)
        # GetPosition
        self.getPositionWithCovariance()
        # GetLinerVelocity
        self.getLinearVelocityWithCovariance()
        # GetAccBias
        self.getAccBias()
        
        self.resetFilter()

    def jitInit(self):
        '''
        Function to get all functions in class jit compiled
        '''

        self.__dryRun()
        self.resetFilter()



