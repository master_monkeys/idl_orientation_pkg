

import numpy as np
import time

from idl_orientation_pkg.JITextendedKalmanFilterSplit import InsEkf
from idl_orientation_pkg.JITextendedKalmanFilterParameters import InsParameters
from idl_transform_pkg.JITdroneConfiguration import DroneGeometry, Rot



"""
class Rot(object):

    def __init__(self):
        pass

    def rotX(self, arg):
        '''
        Rotates the frame

        input   arg is a scalar
        output  is a np mat with shape (3,3)
        '''

        # pre calculates cos and sine
        c = np.cos(arg)
        s = np.sin(arg)

        return np.array([   [ 1, 0, 0],
                            [ 0, c,-s],
                            [ 0, s, c]])

    def rotY(self, arg):
        '''
        Rotates the frame

        input   arg is a scalar
        output  is a np mat with shape (3,3)
        '''

        # pre calculates cos and sine
        c = np.cos(arg)
        s = np.sin(arg)

        return np.array([   [ c, 0, s],
                            [ 0, 1, 0],
                            [-s, 0, c]])

    def rotZ(self, arg):
        '''
        Rotates the frame

        input   arg is a scalar
        output  is a np mat with shape (3,3)
        '''

        # pre calculates cos and sine
        c = np.cos(arg)
        s = np.sin(arg)

        return np.array([   [ c,-s, 0],
                            [ s, c, 0],
                            [ 0, 0, 1]])

    def rotXYZ(self, arg):

        '''
        Rotates Rx*Ry*Rz (xyz)

        input   arg is numpy array with shape (3,1)
        output  is a np mat with shape (3,3)
        '''
        
        # Parses data
        phi     = arg[0, 0]
        theta   = arg[1, 0]
        psi     = arg[2, 0]
        
        return self.rotX(phi) @ self.rotY(theta) @ self.rotZ(psi)

    def rotZYX(self, arg):

        '''
        Rotates Rz*Ry*Rx (zyx)

        input   arg is numpy array with shape (3,1)
        output  is a np mat with shape (3,3)
        '''
        
        # Parses data
        phi     = arg[0, 0]
        theta   = arg[1, 0]
        psi     = arg[2, 0]
        
        return self.rotZ(psi) @ self.rotY(theta) @ self.rotX(phi)


class DroneGeometry(object):

    def __init__(self):
        '''
        convention:
        trans_a_bc is a vector resolved in a describing some measure from b to c

        tHeta_bc is a vector of euler angle describing the relation from frame b to frame c

        rotMat_bc is a DCM matrix describing the relation ship from frame b to frame c... i.e c(tHeta_bc) gives a dcm from frame b to c

        rotFun_xy returns a rot mat

        
        Imu_main frame is denoted u
        Imu_aux frame is denoted w
        Imu_aux_virtual frame is denoted v

        Camera frame is denoted c

        Level frame is denoted l (this frame is a frame with it origin in body, and z in global ned z direction)

        Body (base_link) frame is denoted b
        '''

        # Creates instance of rotation class
        self.__rotation = Rot()


        ########################### World stuff ###########################

        # from global to map
        self.tHeta_nm = np.array([[0.0],[np.pi],[-np.pi/2]], dtype = np.float32)
        self.pos_n_nm = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)


        ########################### Drone stuff ###########################
        ### offsets and misc

        # Camera tilt is the angle the camera is tilted downwards, i.e. if pointing towards the sky the tilt angle is negative
        # Also remember to change in .sdf file... not automated... :-( yet
        self.camera_tilt = 15*np.pi/180


        ### Geometric vectors
        # body to imu main
        self.pos_b_bu = np.array([[0.0],[0.0],[-0.1]], dtype = np.float32)

        # body to imu aux
        self.pos_b_bv = np.array([[0.1],[0.0],[0.0]], dtype = np.float32)
        self.pos_b_bw = self.pos_b_bv

        # body to camera
        self.pos_b_bc = np.array([[0.1],[0.0],[0.0]], dtype = np.float32)
        
        
        
        
        ### Rotation "vector" in sequence zyx with elements [phi theta psi]
        # body to imu main
        self.tHeta_bu = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)

        # body to imu aux (virtual frame)
        self.tHeta_bv = np.array([[0.0],[0.0],[0.0]], dtype = np.float32)
        

        # Body to camera (assumes zed mini imu is in same orientation as camera frame)
        self.tHeta_bc = np.array([[np.pi/2-self.camera_tilt],[0.0],[np.pi/2]], dtype = np.float32)
        self.tHeta_bw = self.tHeta_bc


        # Associated dcm
        self.rotMat_bu = self.__rotation.rotZYX(self.tHeta_bu)
        self.rotMat_bv = self.__rotation.rotZYX(self.tHeta_bv)
        self.rotMat_bc = self.__rotation.rotZYX(self.tHeta_bc)
        self.rotMat_bw = self.rotMat_bc
        
        self.rotMat_nm = self.__rotation.rotZYX(self.tHeta_nm)


    def rotFun_nb(self, tHeta_nb):
        '''
        Function to return dcm from ned to body given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]
        psi     = tHeta_nb[2, 0]


        return self.__rotation.rotX(-phi) @ self.__rotation.rotY(-theta) @ self.__rotation.rotZ(-psi)

    def rotFun_bn(self, tHeta_nb):
        '''
        Function to return dcm from body to ned given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]
        psi     = tHeta_nb[2, 0]


        return self.__rotation.rotZ(psi) @ self.__rotation.rotY(theta) @ self.__rotation.rotX(phi)

    def rotFun_bl(self, tHeta_nb):
        '''
        Function to return dcm from body to level given euler angle vector

        Input with np shape (3,1)
        '''

        # Parsing data
        phi     = tHeta_nb[0, 0]
        theta   = tHeta_nb[1, 0]
        

        return self.__rotation.rotY(theta) @ self.__rotation.rotX(phi) 

    def rateTransform_nb(self, arg):
        '''
        Input is a np mat of shape (3,1)

        Output is a np mat of shape (3,3)

        Transforms body rates to global rates
        '''
        
        # Parses data
        phi     = arg[0][0]
        theta   = arg[1][0]

        # Pre calculating cos, sin and tan
        cx = np.cos(phi)
        cy = np.cos(theta)
        sx = np.sin(phi)
        ty = np.tan(theta)

        # Defines transform
        t = np.array([  [1, sx*ty,  cx*ty],
                        [0,    cx,    -sx],
                        [0, sx/cy,  cx/cy]])

        return t


# Ekf parameter struct (class :o )
class InsParameters(object):
    
    def __init__(self):

        ## System state predict uncertainty matrix
        self.qPosition   = 1.0
        self.qAngles     = 1.0
        self.qLinVel     = 1.0
        self.qAngVel     = 1.0
        self.qLinAcc     = 1.0
        self.qAngAcc     = 1.0
        self.qBiasAcc    = 1.0
        self.qBiasGyro   = 1.0

        ## Measurement uncertainties covariance
        self.rYaw   = 1.0
        self.rPos   = 1.0
        self.rAcc   = 1.0
        self.rGyro  = 1.0
        self.rLevel = 1.0

        ## Sensor orientation relative to estimation frame orientation(Ned)
        self.rotMat_bs = np.eye(3, dtype = np.float32)
        self.pos_b_bs = np.ones((3,1), dtype = np.float32)

        ## Initial conditions
        self.nStates = 24

        self.initState = np.zeros((self.nStates,1), dtype = np.float32)
        self.initCovVec = np.ones(self.nStates, dtype = np.float32)
        self.initCov = np.diag(self.initCovVec)

# EKF class
class InsEkf(object):

    def __init__(self, dt):

        ## System Number of states
        self.__nStates = 24

        ## Rate of predict
        self.__dt = dt

        ## Filter settings
        # Sets how much the abs acc measurement can deviate from g before switching to high g measurement function
        self.__accHighThreshold = 0.1     # percent of g

        # Initialization of filter
        self.__stateInitialization = 0

        ## System state vector
        # [ 0,  1,  2,   3,     4,   5, 6, 7, 8, 9,10,11, 12, 13, 14, 15, 16, 17,  18,  19,  20, 21, 22, 23]
        # [xn, xy, xz, phi, theta, psi, u, v, w, p, q, r, ud, vd, wd, pd, qd, rd, abx, aby, abz, pb, qb, rb]
        self.__x = np.zeros((self.__nStates,1), dtype=np.float32)
        
        ## Predict and state covariance
        # System state covariance matrix
        self.__P = np.eye(self.__nStates, dtype=np.float32)

        ## System state predict uncertainty matrix
        self.__Q = np.eye(self.__nStates, dtype=np.float32)
        
        ## Measurement uncertainties covariance

        # Matrixes
        self.__R_yaw        = 1.0
        self.__R_pos        = np.eye(3, dtype=np.float32)
        self.__R_acc        = np.eye(3, dtype=np.float32)
        self.__R_leveling   = np.eye(2, dtype=np.float32)
        self.__R_omega      = np.eye(3, dtype=np.float32)

        ## Measurement functions
        I3 = np.eye(3, dtype = np.float32)

        # Imu
        hImu = np.zeros((6,self.__nStates), dtype = np.float32)

        # Acceleration true
        hImu[0:3, 12:15] = I3
        # Acceleration bias
        hImu[0:3, 18:21] = I3

        # Angular rate
        hImu[3:6, 9:12] = I3
        # Angular rate bias
        hImu[3:6, 21:24] = I3
        self.__hImu = hImu

        # leveling
        hLeveling = np.zeros((2,self.__nStates), dtype = np.float32)

        # Phi
        hLeveling[0,3] = 1
        # Theta
        hLeveling[1,4] = 1
        self.__hLeveling = hLeveling

        # position
        hPosition = np.zeros((4, self.__nStates), dtype = np.float32)

        # Position
        hPosition[0:3, 0:3] = I3
        # Yaw
        hPosition[3,5] = 1
        self.__hPosition = hPosition

        ## Geometry and transforms
        # Creates a Drone geometry object
        self.__droneGeom = DroneGeometry()

        # Transforms from map to ned frame
        self.__rotMat_nm = self.__droneGeom.rotMat_nm
        self.__rotMat_mn = self.__rotMat_nm.T

        # Transform from body to ned, this is computed and written to self each predict step
        self.__rotMat_nb = np.eye(3, dtype = np.float32)

        # Transform from sensor to estimation frame (body/Ned typically)
        self.__rotMat_bs = np.zeros(3, dtype = np.float32)
        self.__pos_b_bs = np.zeros(3, dtype = np.float32)

        # Gravity
        gravity = 9.81
        self.__gVec = np.array([[0.0],[0.0],[-gravity]], dtype = np.float32)

    def predict(self):
        # Calculates curret linearization of the state transition equation
        F = self.__stateTransitionLin()
        Ft = np.transpose(F)

        # Predicts state
        self.__x = F @ self.__x

        # Wrapping euler angles
        self.__wrapEulerAngles()

        # Predicts state covariance matrix
        self.__P = F @ self.__P @ Ft + self.__Q

    def __wrapEulerAngles(self):
        '''
        Function to wrap states
        phi is wrapped to [-pi, pi)
        theta is wrapped to [-pi, pi)
        psi is wrapped to [0, 2*pi)
        '''

        # Setting to state vector
        self.__x[3, 0] = self.__modPiToPi(self.__x[3,0])
        self.__x[4, 0] = self.__modPiToPi(self.__x[4,0])
        self.__x[5, 0] = np.mod(self.__x[5,0], 2*np.pi, casting = 'same_kind')

    def __modPiToPi(self, ang):
        '''
        Function to map a variable to [-pi to pi)

        TODO: Needs a way to handle cases where the angle is grossly wrong
        '''

        # Predefining variable
        angWrapped = 0.0

        # Wrapping
        if ang >= np.pi:
            angWrapped = ang-2*np.pi
        elif ang < -np.pi:
            angWrapped = 2*np.pi+ang
        else:
            angWrapped = ang

        return angWrapped

    def __stateTransitionLin(self):
        '''
            Function to return state transition matrix of shape (25,25)

            Returns
                np shape (25,25)
        '''

        ## Parses data
        # Vector of angles from global to body
        tHeta = self.__x[3:6]

        # Delta time
        dt = self.__dt
        #dt2 = 0.5*dt**2

        # Identity matrix
        I = np.eye(3, dtype = np.float32)

        ## Transforms
        # Rotation from body to global
        c_nb = self.__droneGeom.rotFun_nb(tHeta)
        # Writing to self to be used in measure function, saves computation
        self.__rotMat_nb = c_nb
        # Rate transform from body to global
        t_nb = self.__droneGeom.rateTransform_nb(tHeta)

        ## State transition
        # Setsup state transition matrix
        F = np.eye(self.__nStates, dtype=np.float32)
        
        # Position related
        F[0:3, 6:9] = c_nb*dt
        
        # Angle related
        F[3:6, 9:12] = t_nb*dt

        # Linear velocity related
        F[6:9,12:15] = I*dt

        # Angular velocity related
        F[9:12, 15:18] = I*dt

        return F

    def __innovationLin(self, xMeasure, H):
        '''
        Finds the error between the measurement and the predicted value, given a linear measurement function

        Input is    X_measure np shape (m,1)
                    H np shape (m,nStates)

        Output is   Y np shape (m,1)
        '''
        

        return xMeasure - H @ self.__x

    def __innovationYaw(self, yawMeasure, yawPredict):
        '''
        Function to return "geodesic" innovation on 0 to 2pi maping

        Takes two scalars as input and returns np shape (1,1)
        '''

        # Predefines error
        e = np.zeros((1,1), dtype = np.float32)

        # wraps measurement
        yawMeasure = np.remainder(yawMeasure, 2*np.pi)

        # Computes the two possible solutions
        e1 = yawMeasure - yawPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e[0][0] = e1
        else:
            e[0][0] = e2

        return e

    def __innovationPhiTheta(self,angMeasure, angPredict):
        '''
        Function to return "geodesic" innovation on -pi to pi maping

        Takes two scalars as input and returns scalar
        '''

        # Wraps measurement
        angMeasure = self.__modPiToPi(angMeasure)

        # Predefines error
        e = 0.0

        e1 = angMeasure - angPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        return e

    def __computeKalman(self,H,R):
        '''
        Function to compute kalman gain

        Input       H np shape (n,m)
                    R np shape (n,n)

        Output      K np shape (m,m)
        '''

        # Pre computes transpose of H
        Ht = np.transpose(H)

        S = H @ self.__P @ Ht + R

        return self.__P @ Ht @ np.linalg.inv(S)

    def __update(self,K,y,H):
        '''
        Function to update estimates

        Input       K np shape (m,n)
                    y np shape (n,1)
                    H np shape (m,n)
        '''


        # Updates estimate
        self.__x = self.__x + K @ y
        self.__P = (np.eye(self.__nStates) - K @ H) @ self.__P

    def __imuLeveling(self, acc):
        '''
        Function to do leveling based on imu acceleration data

        Input   acc np shape (3,1)
        '''

        # Measurement function base on pre-computation of phi and theta

        # Gets imu biases from state vector
        #ab = self.__x[18:21]
        
        # Subtracts biases from accelerometer data to get a more correct measuremen of the gravity field in the acc triad
        #am = acc-ab
        am = acc

        ## Calculating phi and theta based on atan2
        # phi
        phiMeasure = np.arctan2(-am[1,0],-am[2,0], dtype = np.float32)
        
        # theta
        den = np.sqrt(am[1,0]**2 + am[2,0]**2)
        thetaMeasure = np.arctan2(am[0,0], den, dtype = np.float32)

        # gets predict value
        phiPredict = self.__x[3,0]
        thetaPredict = self.__x[4,0]

        # Covariance
        R = self.__R_leveling

        ## Kalman stuff
        # Measurement H matrix
        H = self.__hLeveling

        # Innovation
        y = np.zeros((2,1), dtype = np.float32)
        y[0,0] = self.__innovationPhiTheta(phiMeasure, phiPredict)
        y[1,0] = self.__innovationPhiTheta(thetaMeasure, thetaPredict)
        
        # Kalman
        K = self.__computeKalman(H,R)

        # Update
        self.__update(K,y,H)

    def __imuMeasure(self, acc_e, omg_e):
        '''
        Function to do kalman routine on imu data

        Input       imuData np shape (6,1)
        '''

        # Predefines H and R matrix
        H = np.zeros((6,self.__nStates), dtype = np.float32)
        R = np.zeros((6,6), dtype = np.float32)
        
        R[0:3, 0:3] = self.__R_acc
        R[3:6, 3:6] = self.__R_omega
        
        ## Kalman routine
        # H matrix
        H = self.__hImu

        # Innovation, subtracting gravity (earth rotation can also be added in future using same approach)
        imuDisturbance = np.zeros((6,1), dtype = np.float32)

        # transform from ned to body
        gVec_b = np.transpose(self.__rotMat_nb) @ self.__gVec
        imuDisturbance[0:3] = gVec_b
        
        # computing innovation
        imuData = np.zeros((6,1), dtype = np.float32)
        imuData[0:3] = acc_e
        imuData[3:6] = omg_e
        y = self.__innovationLin(imuData, H) - imuDisturbance
        
        # computes the Kalman gain
        K = self.__computeKalman(H,R)

        # Updates states
        self.__update(K,y,H)

    def __setPosMeasurement(self, pose):
        '''
        Function to set pose measurement

        Input       pose np shape (4,1) on form [x, y, z, yaw]'
        '''

        # Gets H matrices and populates a bigger matrix
        H = np.zeros((4,self.__nStates), dtype = np.float32)
        H = self.__hPosition

        # Defines R matrix
        R = np.zeros((4,4), dtype = np.float32)
        R[0:3, 0:3] = self.__R_pos
        R[3,3] = self.__R_yaw

        # Splits pose data
        posMeasure = pose[0:3]
        yawMeasure = pose[3, 0]

        # Predicted yaw
        yawPredict = self.__x[5 ,0]

        ## Kalman routine

        # Innovation
        y = np.zeros((4,1), dtype = np.float32)
        y_pos = self.__innovationLin(posMeasure, H[0:3,0:self.__nStates])
        y_yaw = self.__innovationYaw(yawMeasure, yawPredict)

        # Populates a common innovation vector
        y[0:3]  = y_pos
        y[3, 0] = y_yaw

        # Kalman gain
        K = self.__computeKalman(H,R)

        # Updates
        self.__update(K,y,H)

    def __skew(self, vec):
        '''
        Function to return matrix form of cross product

        Output:     skew mat np shape (3,1)
        '''
        x = vec[0,0]
        y = vec[1,0]
        z = vec[2,0]

        skew = np.array([   [ 0,-z, y],
                            [ z, 0,-x],
                            [-y, x, 0]], dtype = np.float32)

        return skew

    # Public set functions
    def setImuMeasurement(self, imuData_s):
        '''
        Updates Imu measuremen related states
        
        Also executes leveling subroutine if acc is close to gravity

        input   acc np shape (3,1)
                omg np shape (3,1)
        '''
        ## Transforms data from sensor(s) frame to estimation(e) frame
        acc_s = imuData_s[0:3]
        omg_s = imuData_s[3:6]
        acc_b   = self.__rotMat_bs @ acc_s
        omg_b  = self.__rotMat_bs @ omg_s

        ## Imu measure
        self.__imuMeasure(acc_b, omg_b)
        
        ## Leveling
        self.__imuLeveling(acc_b)

    def setPosMeasurement(self, pose_n):
        '''
        Function to set pose measurement in ned frame

        Input       pose np shape (4,1) on form [x, y, z, yaw]'
        '''

        # Splits data
        pos_n_nb = pose_n[0:3]
        yaw_n = pose_n[3,0]

        pos_n_ns = pos_n_nb + self.__rotMat_nb.T @ self.__pos_b_bs

        # Stitches together pose data
        pose_n = np.zeros((4,1), dtype = np.float32)
        pose_n[0:3] = pos_n_ns
        pose_n[3,0] = yaw_n

        self.__setPosMeasurement(pose_n)

    def setFilterParameters(self, params):
        '''
        Function used to set filter parameters, this function should be ran once afther creating the ins object
        (Alt, can be called in init function of this classes construction, and have param class as init parameter)
        '''

        # Measurement uncertainty covariance
        self.__R_yaw        = params.rYaw
        self.__R_pos        = params.rPos*np.eye(3, dtype=np.float32)
        self.__R_acc        = params.rAcc*np.eye(3, dtype=np.float32)
        self.__R_leveling   = params.rLevel*np.eye(2, dtype=np.float32)
        self.__R_omega      = params.rGyro*np.eye(3, dtype=np.float32)

        # State transition covariance matrix
        Q = np.eye(self.__nStates, dtype = np.float32)
        Q[0:3,0:3]       = params.qPosition*np.eye(3, dtype = np.float32)
        Q[3:6,3:6]       = params.qAngles*np.eye(3, dtype = np.float32)
        Q[6:9,6:9]       = params.qLinVel*np.eye(3, dtype = np.float32)
        Q[9:12,9:12]     = params.qAngVel*np.eye(3, dtype = np.float32)
        Q[12:15,12:15]   = params.qLinAcc*np.eye(3, dtype = np.float32)
        Q[15:18,15:18]   = params.qAngAcc*np.eye(3, dtype = np.float32)
        Q[18:21,18:21]   = params.qBiasAcc*np.eye(3, dtype = np.float32)
        Q[21:24,21:24]   = params.qBiasGyro*np.eye(3, dtype = np.float32)
        # Sets to self variable
        self.__Q = Q

        # Sensor to estimation frame transform
        self.__rotMat_bs = params.rotMat_bs
        self.__pos_b_bs = params.pos_b_bs

        # Initial conditions and covariance
        self.__x = params.initState
        self.__P = params.initCov

    # Public get functions
    def getPosition(self):
        '''
        Function to get the estimated pose in ned frame

        Output:     pos_m np shape (3,1)
        '''

        pos_n_nb = self.__x[0:3] - self.__rotMat_nb.T @ self.__pos_b_bs

        # Returns in map frame
        return pos_n_nb

    def getOrientation(self):
        '''
        Function to get orientation vector tHeta_nb from body to ned

        Output:     tHeta_nb np shape (3,1)
        '''

        # Gets tHeta from states

        return self.__x[3:6]

    def getLinearVelocity(self):
        '''
        Function to get linear velocity in body frame

        This function is a combination of the linear velocity from posFilter, and the angular velocity of the oriFilter

        Output:     vel_n_nb np shape (3,1)
        '''

        # Gets states to use for calculation
        omg_b_ns = self.__x[9:12]
        vel_b_ns = self.__x[6:9]

        return vel_b_ns - self.__skew(omg_b_ns) @ self.__pos_b_bs

    def getAngularVelocity(self):
        '''
        Function to get angular velocity of body frame

        Output:     omg_b_nb np shape (3,1)
        '''

        return self.__x[9:12]

    def getTest(self):
        '''
        Function to get a state that I define, do not use for other then test purpose
        '''
        

        return self.__x[9:12]

# Better EKF class implementation
# Implementing orientation and position as separate filters
class InsOrientationAtan(object):

    def __init__(self, dt):

        ## System Number of states
        self.__nStates = 9

        ## Rate of predict
        self.__dt = dt

        ## Filter settings
        # Sets how much the abs acc measurement can deviate from g before switching to high g measurement function
        self.__accHighThreshold = 0.1     # percent of g

        # Initialization of filter
        self.__stateInitialization = 0

        ## System state vector
        # [  0,     1,   2, 3, 4, 5,  6,  7,  8]
        # [phi, theta, psi, p, q, r, pb, qb, rb]
        self.__x = np.zeros((self.__nStates,1), dtype=np.float32)
        
        ## Predict and state covariance
        # System state covariance matrix
        self.__P = np.eye(self.__nStates, dtype=np.float32)

        ## System state predict uncertainty matrix
        self.__Q = np.eye(self.__nStates, dtype=np.float32)
        

        ## Measurement uncertainties covariance
        # Matrixes
        self.__R_yaw        = 1.0
        self.__R_leveling   = np.eye(2, dtype=np.float32)
        self.__R_omega      = np.eye(3, dtype=np.float32)

        # Measurement functions
        I = np.eye(3, dtype = np.float32)
        self.__hGyro = np.zeros((3,self.__nStates), dtype = np.float32)
        self.__hGyro[0:3, 3:6] = I
        self.__hGyro[0:3, 6:9] = I

        self.__hLeveling = np.zeros((2,self.__nStates), dtype = np.float32)
        self.__hLeveling[0,0] = 1.0
        self.__hLeveling[1,1] = 1.0

        self.__hYaw = np.zeros((1,self.__nStates), dtype = np.float32)
        self.__hYaw[0,2] = 1.0

        ## Geometry and transforms
        # Creates a Drone geometry object
        self.__droneGeom = DroneGeometry()

        # Transform from sensor to estimation frame (body/Ned typically)
        self.__rotMat_bs = np.zeros(3, dtype = np.float32)

    def predict(self):
        # Calculates curret linearization of the state transition equation
        F = self.__stateTransitionLin()
        Ft = np.transpose(F)

        # Predicts state
        self.__x = F @ self.__x

        # Wrapping euler angles
        self.__wrapEulerAngles()

        # Predicts state covariance matrix
        self.__P = F @ self.__P @ Ft + self.__Q

    def __wrapEulerAngles(self):
        '''
        Function to wrap states
        phi is wrapped to [-pi, pi)
        theta is wrapped to [-pi, pi)
        psi is wrapped to [0, 2*pi)
        '''

        ## Setting to state vector
        # Phi
        self.__x[0, 0] = self.__modPiToPi(self.__x[0,0])
        # Theta
        self.__x[1, 0] = self.__modPiToPi(self.__x[1,0])
        # Psi
        self.__x[2, 0] = np.mod(self.__x[2,0], 2*np.pi, casting = 'same_kind')

    def __modPiToPi(self, ang):
        '''
        Function to map a variable to [-pi to pi)

        TODO: Needs a way to handle cases where the angle is grossly wrong
        '''

        # Predefining variable
        angWrapped = 0.0

        # Wrapping
        if ang >= np.pi:
            angWrapped = ang-2*np.pi
        elif ang < -np.pi:
            angWrapped = 2*np.pi+ang
        else:
            angWrapped = ang

        return angWrapped

    def __stateTransitionLin(self):
        '''
            Function to return state transition matrix of shape (25,25)

            Returns
                np shape (25,25)
        '''

        ## Parses data
        # Vector of angles from global to body
        tHeta = self.__x[0:3]

        # Delta time
        dt = self.__dt

        ## Transforms
        # Rate transform from body to global
        t_nb = self.__droneGeom.rateTransform_nb(tHeta)

        ## State transition
        F = np.eye(self.__nStates, dtype=np.float32)
        
        # Angle related
        F[0:3, 3:6] = t_nb*dt

        return F

    def __innovationLin(self, xMeasure, H):
        '''
        Finds the error between the measurement and the predicted value, given a linear measurement function

        Input is    X_measure np shape (m,1)
                    H np shape (m,nStates)

        Output is   Y np shape (m,1)
        '''
        

        return xMeasure - H @ self.__x

    def __innovationYaw(self, yawMeasure, yawPredict):
        '''
        Function to return "geodesic" innovation on 0 to 2pi maping

        Takes two scalars as input and returns np shape (1,1)
        '''

        # Predefines error
        e = np.zeros((1,1), dtype = np.float32)

        # wraps measurement
        yawMeasure = np.remainder(yawMeasure, 2*np.pi)

        # Computes the two possible solutions
        e1 = yawMeasure - yawPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e[0][0] = e1
        else:
            e[0][0] = e2

        return e

    def __innovationPhiTheta(self,angMeasure, angPredict):
        '''
        Function to return "geodesic" innovation on -pi to pi maping

        Takes two scalars as input and returns scalar
        '''

        # Wraps measurement
        angMeasure = self.__modPiToPi(angMeasure)

        # Predefines error
        e = 0.0

        e1 = angMeasure - angPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        return e

    def __computeKalman(self,H,R):
        '''
        Function to compute kalman gain

        Input       H np shape (n,m)
                    R np shape (n,n)

        Output      K np shape (m,m)
        '''

        # Pre computes transpose of H
        Ht = np.transpose(H)

        S = H @ self.__P @ Ht + R

        return self.__P @ Ht @ np.linalg.inv(S)

    def __update(self,K,y,H):
        '''
        Function to update estimates

        Input       K np shape (m,n)
                    y np shape (n,1)
                    H np shape (m,n)
        '''


        # Updates estimate
        self.__x = self.__x + K @ y
        self.__P = (np.eye(self.__nStates) - K @ H) @ self.__P

    def __angleLeveling(self, acc):
        '''
        Function to do leveling based on imu acceleration data

        Input   acc np shape (3,1)
        '''

        ## Calculating phi and theta based on atan2
        # phi
        phiMeasure = np.arctan2(-acc[1,0],-acc[2,0], dtype = np.float32)
        
        # theta
        den = np.sqrt(acc[1,0]**2 + acc[2,0]**2)
        thetaMeasure = np.arctan2(acc[0,0], den, dtype = np.float32)

        # gets predict value
        phiPredict = self.__x[0,0]
        thetaPredict = self.__x[1,0]

        # Covariance
        R = self.__R_leveling

        ## Kalman stuff
        # Measurement H matrix
        H = self.__hLeveling

        # Innovation
        y = np.zeros((2,1), dtype = np.float32)
        y[0,0] = self.__innovationPhiTheta(phiMeasure, phiPredict)
        y[1,0] = self.__innovationPhiTheta(thetaMeasure, thetaPredict)
        
        # Kalman
        K = self.__computeKalman(H,R)

        # Update
        self.__update(K,y,H)

    def __gyroMeasure(self, gyro):
        '''
        Function to do kalman routine on imu data

        Input       imuData np shape (6,1)
        '''

        # Predefines H and R matrix
        H = np.zeros((3,self.__nStates), dtype = np.float32)
        R = np.zeros((3,3), dtype = np.float32)
        
        R[0:3, 0:3] = self.__R_omega
        
        ## Kalman routine
        # H matrix
        H = self.__hGyro

        ## Innovation (earth rotation can also be added in future using same approach)
        gyroDisturbance = np.zeros((3,1), dtype = np.float32)

        # transform from ned to body
        '''Put earth rotation here'''
        
        # computing innovation
        y = self.__innovationLin(gyro, H) - gyroDisturbance
        
        # computes the Kalman gain
        K = self.__computeKalman(H,R)

        # Updates states
        self.__update(K,y,H)

    def __yawMeasurement(self, yawMeasure):
        '''
        Function to do kalman routine on imu data

        Input       imuData np shape (6,1)
        '''

        # Predefines H and R matrix
        H = np.zeros((1,self.__nStates), dtype = np.float32)
        R = np.zeros((1), dtype = np.float32)
        
        R[0:0] = self.__R_yaw
        
        ## Kalman routine
        # H matrix
        H = self.__hYaw

        # Defines yaw as np array with shape (1,1)

        # computing innovation
        yawPredict = self.__x[2,0]
        y = self.__innovationYaw(yawMeasure, yawPredict)
        
        # computes the Kalman gain
        K = self.__computeKalman(H,R)

        # Updates states
        self.__update(K,y,H)

    # Public set functions
    def setImuMeasurement(self, acc_s, omg_s):
        '''
        Updates Imu measuremen related states
        
        Also executes leveling subroutine if acc is close to gravity

        input   acc np shape (3,1)
                omg np shape (3,1)
        '''
        ## Transforms data from sensor(s) frame to estimation(e) frame
        acc_e   = self.__rotMat_bs @ acc_s
        omg_e  = self.__rotMat_bs @ omg_s

        ## Leveling
        self.__angleLeveling(acc_e)

        ## Gyro measure
        self.__gyroMeasure(omg_e)

    def setYawMeasurement(self,yaw_n):
        '''
        Function to set and update yaw measurement

        input is a scalar
        '''

        # Sets measurement
        self.__yawMeasurement(yaw_n)

    def setFilterParameters(self, params):
        '''
        Function used to set filter parameters, this function should be ran once afther creating the ins object
        (Alt, can be called in init function of this classes construction, and have param class as init parameter)
        '''

        # Measurement uncertainty covariance
        self.__R_yaw        = params.rYaw
        self.__R_leveling   = params.rLevel*np.eye(2, dtype=np.float32)
        self.__R_omega      = params.rGyro*np.eye(3, dtype=np.float32)

        # State transition covariance matrix
        Q = np.eye(self.__nStates, dtype = np.float32)
        Q[0:3, 0:3] = params.qAngles*np.eye(3, dtype = np.float32)
        Q[3:6, 3:6] = params.qAngVel*np.eye(3, dtype = np.float32)
        Q[6:9, 6:9] = params.qBiasGyro*np.eye(3, dtype = np.float32)
        # Sets to self variable
        self.__Q = Q

        # Sensor to estimation frame transform
        self.__rotMat_bs = params.rotMat_bs

    # Public get functions
    def getOrientation(self):
        '''
        Function to get orientation vector tHeta_nb from body to ned

        Output:     tHeta_nb np shape (3,1)
        '''

        # Gets tHeta from states

        return self.__x[0:3]

    def getAngularVelocity(self):
        '''
        Function to get angular velocities in estimation frame

        Output:     omg_e_nb np shape (3,1)
                    (e (estimation) frame is typically aligned with body frame)
        '''

        return self.__x[3:6]

    def getTest(self):
        '''
        Function to get a state that I define, do not use for other then test purpose
        '''
        

        return self.__x[3:6]

# EKF class
class InsPosition(object):

    def __init__(self, dt):

        ## System Number of states
        self.__nStates = 12

        ## Rate of predict
        self.__dt = dt

        ## Filter settings

        # Initialization of filter
        self.__stateInitialization = 0

        ## System state vector
        # [0, 1, 2, 3, 4, 5,  6,  7,  8,   9,  10,  11]
        # [x, y, z, u, v, w, ud, vd, wd, abx, aby, abz]
        self.__x = np.zeros((self.__nStates,1), dtype=np.float32)

        ## Orientation
        self.__tHeta = np.zeros((3,1), dtype = np.float32)
        
        ## Predict and state covariance
        # System state covariance matrix
        self.__P = np.eye(self.__nStates, dtype=np.float32)

        ## System state predict uncertainty matrix
        self.__Q = np.eye(self.__nStates, dtype=np.float32)
        

        ## Measurement uncertainties covariance

        # Matrixes
        self.__R_pos        = np.eye(3, dtype=np.float32)
        self.__R_acc        = np.eye(3, dtype=np.float32)

        # Measure functions
        I = np.eye(3, dtype = np.float32)
        self.__hPosition = np.zeros((3,self.__nStates), dtype = np.float32)
        self.__hPosition[0:3, 0:3] = I

        self.__hAcc = np.zeros((3,self.__nStates), dtype = np.float32)
        self.__hAcc[0:3, 6:9]  = I
        self.__hAcc[0:3, 9:12] = I

        ## Geometry and transforms
        # Creates a Drone geometry object
        self.__droneGeom = DroneGeometry()

        # Transforms from map to ned frame
        self.__rotMat_nm = self.__droneGeom.rotMat_nm
        self.__rotMat_mn = self.__rotMat_nm.T

        # Transform from body to ned, this is computed and written to self each predict step
        self.__rotMat_nb = np.eye(3, dtype = np.float32)

        # Transform from sensor to estimation frame (body/Ned typically)
        self.__rotMat_bs = np.zeros(3, dtype = np.float32)

        self.__pos_b_bs = np.zeros((3,1), dtype = np.float32)

        # Gravity
        gravity = 9.81
        self.__gVec = np.array([[0.0],[0.0],[-gravity]], dtype = np.float32)

    def predict(self):
        # Calculates curret linearization of the state transition equation
        F = self.__stateTransitionLin()
        Ft = np.transpose(F)

        # Predicts state
        self.__x = F @ self.__x

        # Predicts state covariance matrix
        self.__P = F @ self.__P @ Ft + self.__Q

    def __stateTransitionLin(self):
        '''
            Function to return state transition matrix of shape (25,25)

            Returns
                np shape (12,12)
        '''


        # Delta time
        dt = self.__dt

        # Identity matrix
        I = np.eye(3, dtype = np.float32)

        ## Transforms
        # Rotation from body to global
        c_nb = self.__droneGeom.rotFun_nb(self.__tHeta)
        # Writing to self to be used in measure function, saves computation
        self.__rotMat_nb = c_nb

        ## State transition
        # Setsup state transition matrix
        F = np.eye(self.__nStates, dtype=np.float32)
        
        # Position related
        F[0:3, 6:9] = c_nb*dt
        
        # Linear velocity related
        F[3:6,6:9] = I*dt

        return F

    def __innovationLin(self, xMeasure, H):
        '''
        Finds the error between the measurement and the predicted value, given a linear measurement function

        Input is    X_measure np shape (m,1)
                    H np shape (m,nStates)

        Output is   Y np shape (m,1)
        '''
        
        return xMeasure - H @ self.__x

    def __computeKalman(self,H,R):
        '''
        Function to compute kalman gain

        Input       H np shape (n,m)
                    R np shape (n,n)

        Output      K np shape (m,m)
        '''

        # Pre computes transpose of H
        Ht = np.transpose(H)

        S = H @ self.__P @ Ht + R

        return self.__P @ Ht @ np.linalg.inv(S)

    def __update(self,K,y,H):
        '''
        Function to update estimates

        Input       K np shape (m,n)
                    y np shape (n,1)
                    H np shape (m,n)
        '''


        # Updates estimate
        self.__x = self.__x + K @ y
        self.__P = (np.eye(self.__nStates) - K @ H) @ self.__P

    def __accMeasure(self, accMeasure):
        '''
        Function to do kalman routine on imu data

        Input       accMeasure np shape (3,1)
        '''

        # Predefines H and R matrix        
        R = self.__R_acc
        
        ## Kalman routine
        # H matrix
        H = self.__hAcc

        # Innovation, subtracting gravity (earth rotation can also be added in future using same approach)
        imuDisturbance = np.transpose(self.__rotMat_nb) @ self.__gVec
        
        # computing innovation
        y = self.__innovationLin(accMeasure, H) - imuDisturbance
        
        # computes the Kalman gain
        K = self.__computeKalman(H,R)

        # Updates states
        self.__update(K,y,H)

    def __posMeasurement(self, posMeasure):
        '''
        Function to set pose measurement

        Input       pose np shape (3,1) on form [x, y, z]'
        '''

        # Gets H matrices and populates a bigger matrix
        H = self.__hPosition

        # Defines R matrix
        R = self.__R_pos

        ## Kalman routine

        # Innovation
        y = self.__innovationLin(posMeasure, H)

        # Kalman gain
        K = self.__computeKalman(H,R)

        # Updates
        self.__update(K,y,H)

    # Public set functions
    def setAccMeasurement(self,accMeasure):
        '''
        Updates Imu measuremen related states
        
        Also executes leveling subroutine if acc is close to gravity

        input   imuData np shape (3,1) with elements [ax ay az]'
        '''
        ## Transforms data from sensor(s) frame to estimation(e) frame
        accMeasure_e = self.__rotMat_bs @ accMeasure

        ## Imu measure
        self.__accMeasure(accMeasure_e)

    def setPosMeasurement(self, pos_n_nb):
        '''
        Function to set pose measurement in map frame

        Input       pose np shape (3,1) on form [x y z]'
        '''

        pos_n_ns = pos_n_nb + self.__rotMat_nb.T @ self.__pos_b_bs

        self.__posMeasurement(pos_n_ns)

    def setOrientation(self, tHeta):
        '''
        Function to set orientation of imu

        Input tHeta with np shape (3,1)
        '''

        self.__tHeta = tHeta

    def setFilterParameters(self, params):
        '''
        Function used to set filter parameters, this function should be ran once afther creating the ins object
        (Alt, can be called in init function of this classes construction, and have param class as init parameter)
        '''

        # Measurement uncertainty covariance
        self.__R_pos        = params.rPos*np.eye(3, dtype=np.float32)
        self.__R_acc        = params.rAcc*np.eye(3, dtype=np.float32)

        # State transition covariance matrix
        Q = np.eye(self.__nStates, dtype = np.float32)
        Q[0:3,0:3]      = params.qPosition*np.eye(3, dtype = np.float32)
        Q[3:6,3:6]      = params.qLinVel*np.eye(3, dtype = np.float32)
        Q[6:9,6:9]      = params.qLinAcc*np.eye(3, dtype = np.float32)
        Q[9:12,9:12]    = params.qBiasAcc*np.eye(3, dtype = np.float32)

        # Sets to self variable
        self.__Q = Q

        # Sensor to estimation frame transform
        self.__rotMat_bs = params.rotMat_bs
        self.__pos_b_bs = params.pos_b_bs

    # Public get functions
    def getPosition(self):
        '''
        Function to get the estimated pose in map frame

        Output:     pos_n_nb np shape (3,1)
        '''

        # Gets pos in ned frame from state
        pos_n_nb = self.__x[0:3] - self.__rotMat_nb.T @ self.__pos_b_bs

        # Returns in map frame
        return pos_n_nb

    def getLinearVelocity(self):
        '''
        Function to get linear velocity in estimation frame (typically aligned with body frame)

        Output:     vel_b_ns np shape (3,1)
        '''

        return self.__x[3:6]

    def getAccBias(self):
        '''
        Function to get acc bias

        Output with np shape (3,1)
        '''

        return self.__x[9:12]

# Holistic INS ekf class
class InsFilter(object):

    def __init__(self, dtPos, dtOri):

        # Creates orientation and position filtering objects
        self.__orientationFilter = InsOrientationAtan(dtOri)

        self.__positionFilter = InsPosition(dtPos)

        # Geometry data
        self.__pos_b_bs = np.zeros((3,1), dtype = np.float32)

    def predict(self):
        '''
        Function to call predict function of both part filters at the same time
        '''

        # Calls both predict functions
        self.predictOrientation()
        self.predictPosition()

    def predictOrientation(self):
        '''
        Function to predict orientation filter
        '''

        self.__orientationFilter.predict()

    def predictPosition(self):
        '''
        Function to predict position
        '''

        # Gets tHeta from orientation filter and sets it to the position filter
        self.__positionFilter.setOrientation(self.__orientationFilter.getOrientation())
        
        # Predicts
        self.__positionFilter.predict()

    def __skew(self, vec):
        '''
        Function to return matrix form of cross product

        Output:     skew mat np shape (3,1)
        '''
        x = vec[0,0]
        y = vec[1,0]
        z = vec[2,0]

        skew = np.array([   [ 0,-z, y],
                            [ z, 0,-x],
                            [-y, x, 0]], dtype = np.float32)

        return skew

    # Sensor and aiding functions
    def setImuMeasurement(self, imuData):
        '''
        Function to set imu data

        Input imu data np shape (6,1) on form [ax ay az wx wy wz]'
        '''
        # Splits data to acc and omg part
        accMeasure = imuData[0:3]
        omgMeasure = imuData[3:6]

        # Gets imu bias
        accBias = self.__positionFilter.getAccBias()

        # Sets data to orientation filter
        accTrue = accMeasure
        self.__orientationFilter.setImuMeasurement(accTrue, omgMeasure)

        # Sets data to
        self.__positionFilter.setAccMeasurement(accMeasure)

    def setPosMeasurement(self, pose):
        '''
        Function to set pose in ned frame

        Input pose np shape (4,1) in map frame
        '''

        # Splits pose to pos and yaw
        posMeasurement = pose[0:3]
        yawMeasurement = pose[3,0]

        # Sets yaw to orientation filter
        self.__orientationFilter.setYawMeasurement(yawMeasurement)

        # Sets pose to position filter
        self.__positionFilter.setPosMeasurement(posMeasurement)

    # Set filter params
    def setFilterParameters(self, params):
        '''
        Function to set filter parameters
        '''

        # Passes to member objects
        self.__orientationFilter.setFilterParameters(params)
        self.__positionFilter.setFilterParameters(params)

        # Sets to self variables
        self.__pos_b_bs = params.pos_b_bs

    # Public get functions
    def getPosition(self):
        '''
        Function to get position in map frame

        Output  position np shape (3,1)
        '''

        return self.__positionFilter.getPosition()

    def getOrientation(self):
        '''
        Function to get orientation

        Output  orientation np shape (3,1)
        '''

        return self.__orientationFilter.getOrientation()

    def getLinearVelocity(self):
        '''
        Function to get linear velocity in body frame

        This function is a combination of the linear velocity from posFilter, and the angular velocity of the oriFilter

        Output:     vel_n_nb np shape (3,1)
        '''

        # Gets states to use for calculation
        omg_b_ns = self.__orientationFilter.getAngularVelocity()
        vel_b_ns = self.__positionFilter.getLinearVelocity()

        return vel_b_ns - self.__skew(omg_b_ns) @ self.__pos_b_bs

    def getAngularVelocity(self):
        '''
        Function to get angular velocity of body frame

        Output:     omg_b_nb np shape (3,1)
        '''

        return self.__orientationFilter.getAngularVelocity()

    def getTest(self):
        return self.__orientationFilter.getTest()

"""

def main():
    
    ## Setting up INS params
    droneGeom = DroneGeometry()
    insMainParam = InsParameters()

    # System state predict uncertainty matrix
    insMainParam.qPosition   = 0.01
    insMainParam.qAngles     = 0.01
    insMainParam.qLinVel     = 0.01
    insMainParam.qAngVel     = 0.1
    insMainParam.qLinAcc     = 0.1
    insMainParam.qAngAcc     = 0.1
    insMainParam.qBiasAcc    = 0.001
    insMainParam.qBiasGyro   = 0.001

    # Measurement uncertainties covariance
    insMainParam.rYaw   = 0.1
    insMainParam.rPos   = 0.1
    insMainParam.rAcc   = 0.01
    insMainParam.rGyro  = 0.01
    insMainParam.rLevel = 0.1

    # geometry
    insMainParam.rotMat_bs = droneGeom.rotMat_bv
    insMainParam.pos_b_bs = droneGeom.pos_b_bv

    
    # Creates test conditions
    droneGeom = DroneGeometry()
    yaw = 0.0
    tHeta_nb = np.array([[0.0],[0.0],[yaw]], dtype = np.float32)
    pose = np.array([[0.0],[0.0],[0.0],[yaw]], dtype = np.float32)
    
    # omg
    omg_b_nb = np.array([[0.0],[0.0],[2.0]], dtype = np.float32)
    t_nb = droneGeom.rateTransform_nb(tHeta_nb)
    omg_n_nb = t_nb @ omg_b_nb

    # acc
    acc_g = np.array([[0.0],[0.0],[-9.81]], dtype = np.float32)
    c_nb = droneGeom.rotFun_nb(tHeta_nb)
    acc_b = c_nb @ acc_g

    ## Creating ins object
    dt = 0.1
    ins = InsEkf(dt)
    print('Class created')
    ins.setFilterParameters(insMainParam)
    print('Nom nom params eaten')

    # Set imu measurement
    imuData = np.zeros((6,1), dtype = np.float32)
    imuData[0:3] = acc_b
    imuData[3:6] = omg_b_nb

    iterations = 1000

    omg_z = 2.0
    yaw = 0.0

    print('Set IMU data')
    ins.setImuMeasurement(imuData)
    print('Set Pos data')
    ins.setPosMeasurement(pose)
    print('Predict')
    ins.predict()
    
    print('Starting timer')
    startOriTime = time.time()
    for ii in range(iterations):


        yaw += omg_z*dt
        omg_b_nb = np.array([[0.0],[0.0],[omg_z]], dtype = np.float32)
        imuData[3:6] = omg_b_nb
        pose = np.array([[0.0],[0.0],[0.0],[yaw]], dtype = np.float32)
        
        ins.setImuMeasurement(imuData)
        ins.setPosMeasurement(pose)
        ins.predict()

    endOriTime = time.time()

    print('Excec. Time')
    print(endOriTime-startOriTime)
    print('Position')
    print(ins.getPosition())
    print('Orientation')
    print(ins.getOrientation())
    print('Lin. vel.')
    print(ins.getLinearVelocity())
    print(ins.getTest())
    



if __name__ == '__main__':
    main()