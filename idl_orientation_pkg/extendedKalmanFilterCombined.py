

import numpy as np


# Import Drone config class
from idl_botsy_pkg.droneConfiguration import DroneGeometry

# Imports Filters and filter params
from idl_orientation_pkg.JITextendedKalmanFilterParameters import InsParameters



# Holistic INS ekf class
class InsEkf(object):

    def __init__(self, dt):

        # Kalman filter
        ## System Number of states
        self.__nStates = 18

        ## Filter settings

        # Filter state
        self.online = True

        # Second order propagation
        self.__secondOrderPredict = True

        # Fixed predict rate
        self.__fixedPredictRate = True
        self.__fixedPredictRateDt = np.float32(dt)

        # Initialization of filter
        self.__stateInitialization = 0

        # Position and ang threshold
        self.__posThreshold = 1.0
        self.__accHighThreshold = 0.01     # percent of g
        self.__gravity = np.float(9.81)
        self.__gVec = np.array([[np.float32(0.0)],[np.float32(0.0)],[-self.__gravity]], dtype = np.float32)
        self.__gravityState = False

        self.__yawThreshold = 1.57

        ## System state vector
        # [0, 1, 2,  3, 4, 5,    6,   7,   8,    9,    10,  11,   12,  13,  14,  15, 16, 17]
        # [x, y, z,  u, v, w,  abx, aby, abz,  phi, theta, psi,  wbx, wby, wbz,  gx, gy, gz]
        self.__x = np.zeros((self.__nStates,1), dtype=np.float32)
        # Initializes gravity vector
        self.__x[17:0] = -self.__gravity
        self.__xInit = self.__x

        # Acceleration
        self.__a = np.zeros((3,1), dtype = np.float32)
        self.__w = np.zeros((3,1), dtype = np.float32)
        self.__aCum = np.zeros((3,1), dtype = np.float32)
        self.__wCum = np.zeros((3,1), dtype = np.float32)

        # Acce measurement
        self.__accMeasure_e = np.array([[np.float32(0.0)],[np.float32(0.0)],[-self.__gravity]], dtype = np.float32)

        # variables for second order predict
        self.__xLast = self.__x
        self.__aLast = self.__a
        self.__wLast = self.__w
        
        ## Predict and state covariance
        # System state covariance matrix
        self.__P = np.eye(self.__nStates, dtype=np.float32)
        self.__PInit = self.__P

        ## System state predict uncertainty matrix
        self.__Q = np.eye(self.__nStates, dtype=np.float32)
        
        ## Measurement uncertainties covariance

        # Matrixes
        self.__R_pos        = np.eye(3, dtype=np.float32)
        self.__R_yaw        = 1.0
        self.__R_leveling   = 1.0

        # Measure functions
        I = np.eye(3, dtype = np.float32)
        self.__hPosition = np.zeros((3,self.__nStates), dtype = np.float32)
        self.__hPosition[0:3, 0:3] = I
        
        self.__hYaw = np.zeros((1,self.__nStates), dtype = np.float32)
        self.__hYaw[0,11] = 1.0

        self.__hLeveling = np.zeros((2,self.__nStates), dtype = np.float32)
        self.__hLeveling[0,9] = 1.0
        self.__hLeveling[1,10] = 1.0

        
        # Geometry data
        self.__droneGeom = DroneGeometry()
        self.__rotMat_bs = np.eye(3, dtype = np.float32)
        self.__pos_b_bs = np.zeros((3,1), dtype = np.float32)

        # Transform from body to ned, this is computed and written to self each predict step
        self.__rotMat_bn = np.eye(3, dtype = np.float32)
        self.__t_nb = np.zeros((3,3), dtype = np.float32)

        # Time stuff
        self.__timeMaxDiffPose = 10.0
        self.__timeNewPoseUpdate = 0.0
        self.__timeDiffPose = self.__timeMaxDiffPose +2.0

        self.__timeLastImuMsg = 0.0
        self.__maxDt = 1.0/50.0

        self.__imuPredictDt = np.float32(self.__maxDt)

        # Delta imu related variables
        self.__deltaImuCounter = 0
        self.__deltaImuDt = 0.001
        self.__deltaImuCum = False

        # Bool
        self.__filterIsNotReSet = True

        print('INS combined alive!')

    def predict(self):
        '''
        If fixed rate predict is active then the predict function is called here
        '''
        if self.__fixedPredictRate == True:
            self.__predictAndLevel()

    def checkTiming(self, currentTime):
        '''
        Function to routinely call to check if timers has expired
        '''

        # Finds delta time            
        self.__timeDiffPose = currentTime - self.__timeNewPoseUpdate

        # If timer expires, then filter is set offline
        if self.__timeDiffPose > self.__timeMaxDiffPose:
            self.online = False
            # Reseting filter once
            if self.__filterIsNotReSet == True:
                self.__filterIsNotReSet = False
                self.resetFilter(keepAngles=True)
                # Printing that filter is offline
                print('Warn: orientation node: pos filter offline')
                print('timer expired by:')
                print(self.__timeDiffPose)
        else:
            self.online = True
            # Reseting filter once
            if self.__filterIsNotReSet == False:
                self.__filterIsNotReSet = True
                print('Status: orientation node: pos filter online!')
    
    
    # Predict functions
    def __predict(self, dt):
        '''
            Used to predict the next filter state

            Input   dt is a scalar
        '''
        # Calculates non linear therms and stores them to selfs
        tHeta_nb = self.__x[9:12]
        self.__rotMat_bn = self.__droneGeom.rotFun_bn(tHeta_nb)
        self.__t_nb = self.__droneGeom.rateTransform_nb(tHeta_nb)

        # Calculates curret linearization of the state transition equation
        F = self.__stateTransitionLin(dt, strangeTherms=False)

        B = self.__controlInput(dt)

        W = self.__covariancePredictWeight(dt)
        
        ## Predicts state
        # If second order, can be used in cases where compute power is limited, ie. predict must be run more seldomly
        if self.__secondOrderPredict == True:
            # Calculates propagation step and control input
            x = np.float32(1.5)*self.__x - np.float32(0.5)*self.__xLast
            a = np.float32(1.5)*self.__a - np.float32(0.5)*self.__aLast
            w = np.float32(1.5)*self.__w - np.float32(0.5)*self.__wLast

            # Calculates correct state propagation matrix for second order step
            f = F - np.eye(self.__nStates, dtype = np.float32)

            # Control input
            u = np.zeros((6,1), dtype = np.float32)
            u[0:3] = a
            u[3:6] = w

            # Propagates
            self.__x = self.__x + f @ x + B @ u

            # Sets last values to current values
            self.__xLast = self.__x
            self.__aLast = self.__a
            self.__wLast = self.__w
        else:
            # Control input
            u = np.zeros((6,1), dtype = np.float32)
            u[0:3] = self.__a
            u[3:6] = self.__w
            
            # Std filter implementation
            self.__x = F @ self.__x + B @ u

        # Predicts state covariance matrix
        self.__P = F @ self.__P @ F.T + W @ self.__Q @ W.T

        # Wrapping euler angles
        self.__wrapEulerAngles()

    def __stateTransitionLin(self, dt, strangeTherms = False):
        '''
            Function to return state transition matrix

            Returns
                np shape (18, 18)
        '''

        # Identity matrix
        I = np.eye(3, dtype = np.float32)

        ## State transition
        # Setsup state transition matrix
        F = np.eye(self.__nStates, dtype=np.float32)
        
        # Position related
        if self.online:
            # Pos related
            # Integration of lin vel
            F[0:3, 3:6] = I*dt
            
            # Vel related
            # Subtraction og bias
            F[3:6, 6:9] = -self.__rotMat_bn*dt
            # Strange therm
            if strangeTherms:
                # Gets data for comp of strange therm
                tHeta = self.__x[9:12]
                am = self.__a
                ab = self.__x[6:9]
                F[3:6,9:12] = self.__velThetaStateTransTherm(tHeta, am, ab)*dt
            # Subtraction of grav acc (specific force from gravity, tho gravity is not a force, but that construct is okay for this filter)
            if self.__gravityState == True:
                F[3:6, 15:18] = -I*dt
        
        # Angle related
        # Subtraction of omg bias
        F[9:12, 12:15] = -self.__t_nb*dt

        # When filter is disabled the yaw is kept constant
        if self.online == False:
            F[11,12:15] = np.zeros((1,3), dtype = np.float32)

        if strangeTherms:
            # Gets data for comp of strange therm
            tHeta = self.__x[9:12]
            wm = self.__w
            wb = self.__x[12:15]
            F[9:12, 9:12] = I + self.__thetaThetaStateTransTherms(tHeta, wm, wb)*dt

        return F

    def __velThetaStateTransTherm(self, tHeta, am, ab):
        '''
        Function to compute strange therm to do with velocity update
        '''
        # Parsing data
        phi = tHeta[0,0]
        theta = tHeta[1,0]
        psi = tHeta[2,0]

        amx = am[0,0]
        amy = am[1,0]
        amz = am[2,0]

        abx = ab[0,0]
        aby = ab[1,0]
        abz = ab[2,0]

        #Pre computes
        sx = np.sin(phi)
        cx = np.cos(phi)
        sy = np.sin(theta)
        cy = np.cos(theta)
        sz = np.sin(psi)
        cz = np.cos(psi)

        mat = np.array([    [ -((sx*sz + cx*cz*sy)*(aby - amy) + (cx*sz - cz*sx*sy)*(abz - amz)), -(cx*cy*cz*(abz - amz) - cz*sy*(abx - amx) + cy*cz*sx*(aby - amy)),  ((cx*cz + sx*sy*sz)*(aby - amy) - (cz*sx - cx*sy*sz)*(abz - amz) + cy*sz*(abx - amx))],
                            [  ((cz*sx - cx*sy*sz)*(aby - amy) + (cx*cz + sx*sy*sz)*(abz - amz)), -(cx*cy*sz*(abz - amz) - sy*sz*(abx - amx) + cy*sx*sz*(aby - amy)), -((sx*sz + cx*cz*sy)*(abz - amz) - (cx*sz - cz*sx*sy)*(aby - amy) + cy*cz*(abx - amx))],
                            [                           -(cx*cy*(aby - amy) - cy*sx*(abz - amz)),           (cy*(abx - amx) + cx*sy*(abz - amz) + sx*sy*(aby - amy)),                                                                        np.float32(0.0)]], dtype = np.float32)

        return mat

    def __thetaThetaStateTransTherms(self, tHeta, wm, wb):
        '''
        Function to compute strange therm to do with tHeta update
        '''

        # Parsing data
        phi = tHeta[0,0]
        theta = tHeta[1,0]
        psi = tHeta[2,0]

        wmx = wm[0,0]
        wmy = wm[1,0]
        wmz = wm[2,0]

        pb = wb[0,0]
        qb = wb[1,0]
        rb = wb[2,0]

        #Pre computes
        sx = np.sin(phi)
        cx = np.cos(phi)
        sy = np.sin(theta)
        cy = np.cos(theta)
        ty = np.tan(theta)
        sz = np.sin(psi)
        cz = np.cos(psi)

        # Guards agains divide by zero
        if np.abs(cy) < 0.01:
            cy = 0.01*np.sign(cy)

        mat = np.array([    [     -(cx*ty*(qb - wmy) - sx*ty*(rb - wmz)), -(cx*(ty**2 + 1)*(rb - wmz) + sx*(ty**2 + 1)*(qb - wmy)), 0],
                            [            (cx*(rb - wmz) + sx*(qb - wmy)),                                                        0, 0],
                            [ -((cx*(qb - wmy))/cy - (sx*(rb - wmz))/cy),   -((cx*sy*(rb - wmz))/cy**2 + (sx*sy*(qb - wmy))/cy**2), 0]], dtype = np.float32)

        return mat

    def __controlInput(self, dt):
        '''
            Function to return control input matrix

            Returns
                np shape (18, 6)
        '''

        # Predefining B matrix
        B = np.zeros((self.__nStates,6), dtype = np.float32)

        # Populates B matrix
        # Acc related
        B[3:6,0:3] = self.__rotMat_bn*dt
        # Omg related
        B[9:12,3:6] = self.__t_nb*dt

        if self.online == False:
            B[11,3:6] = np.zeros((1,3), dtype = np.float32)

        return B

    def __covariancePredictWeight(self, dt):
        '''
            Function to return covariance weight update

            return W np shape (18,18)
        '''

        # Creates eye mat
        W = np.eye(self.__nStates, dtype = np.float32)
        # Sets nonlinear therm
        W[3:6,3:6] = self.__rotMat_bn
        W[9:12,9:12] = self.__t_nb

        # Weights by dt
        W = W*dt

        return W

    def __wrapEulerAngles(self):
        '''
        Function to wrap states
        phi is wrapped to [-pi, pi)
        theta is wrapped to [-pi, pi)
        psi is wrapped to [0, 2*pi)
        '''

        ## Setting to state vector
        # Phi
        self.__x[0, 0] = self.__modPiToPi(self.__x[0,0])
        # Theta
        self.__x[1, 0] = self.__modPiToPi(self.__x[1,0])
        # Psi
        self.__x[2, 0] = np.mod(self.__x[2,0], 2*np.pi)

    # Euler angle specifics
    def __wrapEulerAngles(self):
        '''
        Function to wrap states
        phi is wrapped to [-pi, pi)
        theta is wrapped to [-pi, pi)
        psi is wrapped to [0, 2*pi)
        '''

        ## Setting to state vector
        # Phi
        self.__x[9, 0] = self.__modPiToPi(self.__x[9,0])
        # Theta
        self.__x[10, 0] = self.__modPiToPi(self.__x[10,0])
        # Psi
        self.__x[11, 0] = np.mod(self.__x[11,0], 2*np.pi)

    def __modPiToPi(self, ang):
        '''
        Function to map a variable to [-pi to pi)

        TODO: Needs a way to handle cases where the angle is grossly wrong
        '''

        # Predefining variable
        angWrapped = 0.0

        # Wrapping
        if ang >= np.pi:
            angWrapped = ang-2*np.pi
        elif ang < -np.pi:
            angWrapped = 2*np.pi+ang
        else:
            angWrapped = ang

        return angWrapped

    # Innovation functions
    def __innovationLin(self, xMeasure, H):
        '''
        Finds the error between the measurement and the predicted value, given a linear measurement function

        Input is    X_measure np shape (m,1)
                    H np shape (m,nStates)

        Output is   Y np shape (m,1)
        '''
        
        return xMeasure - H @ self.__x

    def __innovationYaw(self, yawMeasure, yawPredict):
        '''
        Function to return "geodesic" innovation on 0 to 2pi maping

        Takes two scalars as input and returns np shape (1,1)
        '''

        # Predefines error
        e = np.zeros((1,1), dtype = np.float32)

        # wraps measurement
        yawMeasure = np.remainder(yawMeasure, 2*np.pi)

        # Computes the two possible solutions
        e1 = yawMeasure - yawPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e[0][0] = e1
        else:
            e[0][0] = e2

        return e

    def __innovationPhiTheta(self,angMeasure, angPredict):
        '''
        Function to return "geodesic" innovation on -pi to pi maping

        Takes two scalars as input and returns scalar
        '''

        # Wraps measurement
        angMeasure = self.__modPiToPi(angMeasure)

        # Predefines error
        e = 0.0

        e1 = angMeasure - angPredict

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        return e
    
    # Vanilla kf functions
    def __computeKalman(self,H,R):
        '''
        Function to compute kalman gain

        Input       H np shape (n,m)
                    R np shape (n,n)

        Output      K np shape (m,m)
        '''

        S = H @ self.__P @ H.T + R

        return self.__P @ H.T @ np.linalg.inv(S)

    def __update(self,K,y,H):
        '''
        Function to update estimates

        Input       K np shape (m,n)
                    y np shape (n,1)
                    H np shape (m,n)
        '''


        # Updates estimate
        self.__x = self.__x + K @ y
        self.__P = (np.eye(self.__nStates, dtype=np.float32) - K @ H) @ self.__P

    # Position measuremen functions
    def __posMeasurement(self, posMeasure, R):
        '''
        Function to set pose measurement

        Input       pose np shape (3,1) on form [x, y, z]'
        '''

        # Gets H matrices and populates a bigger matrix
        H = self.__hPosition

        ## Kalman routine

        # Innovation
        y = self.__innovationLin(posMeasure, H)

        # Kalman gain
        K = self.__computeKalman(H,R)

        # Updates
        self.__update(K,y,H)

    def __posSet(self, posMeasure):
        '''
        Function to set the position directly, this is to be used when there is a "large" jump in the pf solution

        input posMeasure np shape (3,1)
        '''

        # Sets position to measured position and kills velocity
        self.__x[0:3] = posMeasure
        self.__x[3:6] = np.zeros((3,1), dtype = np.float32)
        self.__x[6:9] = np.zeros((3,1), dtype = np.float32)
        # Sets cov to init cov
        self.__P[0:9] = self.__PInit[0:9] 

    def __posEvaluation(self, posMeasure, R):
        '''
        Function to decide if position is to be passed as a measurement or a direct replacement of the position in the filter

        If position from pf filter jumps the prediction of the pf filter is passed directly to the states to prevent a false velocity spike
        This velocity spike will then move the particles in a wrong direction, further worsening the problem

        Input       pose np shape (3,1) on form [x y z]'
                    R    np shape (3,3)
        '''

        # Gets predicted value
        posPredict = self.__x[0:3]

        # calculates norm predicted to "measurement"
        norm = np.linalg.norm(posMeasure-posPredict)

        if norm <= self.__posThreshold:
            self.__posMeasurement(posMeasure, R)
        else:
            self.__posSet(posMeasure)
            print('PoseSet')

    # Yaw measurement stuff
    def __yawMeasurement(self, yawMeasure, R):
        '''
        Function to do kalman routine on imu data

        Input       Yaw np shape (1,1)
                    R       scalar
        '''
        
        ## Kalman routine
        # H matrix
        H = self.__hYaw

        # Defines yaw as np array with shape (1,1)

        # computing innovation
        yawPredict = self.__x[11,0]
        y = self.__innovationYaw(yawMeasure, yawPredict)
        
        # computes the Kalman gain
        K = self.__computeKalman(H,R)

        # Updates states
        self.__update(K,y,H)

    def __yawSet(self, yawMeasure):
        '''
        Function to set the yaw directly, this is to be used when there is a "large" jump in the pf solution

        input yawMeasure np shape (3,1)
        '''

        # Sets yaw to measured yaw
        self.__x[11,0] = yawMeasure
        self.__x[14,0] = np.float32(0.0)

        # Sets cov to init cov
        self.__P[11,11] = self.__PInit[11,11]
        self.__P[14,14] = self.__PInit[14,14]

    def __yawEvaluation(self, yawMeasure, R):
        '''
        Function to decide if position is to be passed as a measurement or a direct replacement of the position in the filter

        If position from pf filter jumps the prediction of the pf filter is passed directly to the states to prevent a false velocity spike
        This velocity spike will then move the particles in a wrong direction, further worsening the problem

        Input       pose np shape (3,1) on form [x y z]'
                    R    np shape (3,3)
        '''

        # Gets predicted value
        yawPredict = self.__x[11,0]

        # calculates norm predicted to "measurement"
        norm = np.abs(self.__innovationYaw(yawMeasure, yawPredict))

        if norm <= self.__yawThreshold:
            self.__yawMeasurement(yawMeasure, R)
        else:
            self.__yawSet(yawMeasure)
            print('YawSet')

    # Leveling function
    def __angleLeveling(self, acc, subBias = False):
        '''
        Function to do leveling based on imu acceleration data

        Input   acc np shape (3,1)
        '''

        ## Subtracting bias
        if subBias == True:
            accLeveling = acc - self.__x[6:9]
        else:
            accLeveling = acc


        ## Calculating phi and theta based on atan2
        # phi
        phiMeasure = np.arctan2(-accLeveling[1,0],-accLeveling[2,0])
        
        # theta
        den = np.sqrt(accLeveling[1,0]**2 + accLeveling[2,0]**2)
        thetaMeasure = np.arctan2(accLeveling[0,0], den)
        
        # gets predict value
        phiPredict = self.__x[9,0]
        thetaPredict = self.__x[10,0]

        ## Covariance, dynamic tuning of filter
        # Finds deviation from G
        devFromG = np.abs(np.linalg.norm(accLeveling) - self.__gravity)
        # Calculates the absolute covariance
        rScalar = self.__R_leveling*(1.0 + 5000.0*(devFromG + devFromG**2))
        R = rScalar*np.eye(2, dtype = np.float32)
        R = R.astype(np.float32)

        ## Kalman stuff
        # Measurement H matrix
        H = self.__hLeveling

        # Innovation
        y = np.zeros((2,1), dtype = np.float32)
        y[0,0] = self.__innovationPhiTheta(phiMeasure, phiPredict)
        y[1,0] = self.__innovationPhiTheta(thetaMeasure, thetaPredict)
        
        # Kalman
        K = self.__computeKalman(H,R)

        # Update
        self.__update(K,y,H)

    # Set Imu related function
    def __predictAndLevel(self):
        '''
        Function to unite acctions that are to be taken based on IMU data
        '''

        # Uses data for predict
        if self.__deltaImuCum == True and self.__fixedPredictRate == True:
            # Guards against decide by zero
            if self.__deltaImuDt < 0.001:
                self.__deltaImuDt = 0.001
            
            # Devides delta imu msg by accumulated time to bring it back to original imu units
            self.__a = self.__aCum / self.__deltaImuDt
            self.__w = self.__wCum / self.__deltaImuDt
            # Sets delta imu time to dt for predict
            dt = self.__deltaImuDt
            
            # Resets values
            self.__aCum = np.zeros((3,1), dtype = np.float32)
            self.__wCum = np.zeros((3,1), dtype = np.float32)
            self.__deltaImuDt = np.float32(0.0)
            self.__deltaImuCounter = 0
        else:
            dt = self.__imuPredictDt

        # If fixed predict rate is true, then dt is set to the fixed predict rate delta time
        if self.__fixedPredictRate == True:
            dt = self.__fixedPredictRateDt

        # Predicts based on new control data
        self.__predict(dt)

        ## Leveling, only when there is low accelerations
        # Abs of sensor acceleration
        acc_e = self.__accMeasure_e

        absAcc = np.linalg.norm(acc_e)

        # Acceleration thereshold
        epsilon = self.__accHighThreshold
        lowThreshold = self.__gravity*(1-epsilon)
        highThreshold = self.__gravity*(1+epsilon)

        if lowThreshold < absAcc and absAcc < highThreshold:
            self.__angleLeveling(acc_e, subBias= False)
            
    # Helper functions
    def __skew(self, vec):
        '''
        Function to return matrix form of cross product

        Output:     skew mat np shape (3,1)
        '''
        x = vec[0,0]
        y = vec[1,0]
        z = vec[2,0]

        skew = np.array([   [ 0,-z, y],
                            [ z, 0,-x],
                            [-y, x, 0]], dtype = np.float32)

        return skew

    def __rMatBNCovariancePropagation(self, tHeta, pos):
        '''
        Function to return the derivative of the propagation function

        Input       tHeta np shape (3,1)
                    pos   np shape (3,1)
        '''
        # Parsing data
        phi     = tHeta[0,0]
        theta   = tHeta[1,0]
        psi     = tHeta[2,0]

        rx = pos[0,0]
        ry = pos[1,0]
        rz = pos[2,0]

        # Pre computing
        sx = np.sin(phi)
        cx = np.cos(phi)
        sy = np.sin(theta)
        cy = np.cos(theta)
        sz = np.sin(psi)
        cz = np.cos(psi)

        deltaF = np.array([ [  ry*(sx*sz + cx*cz*sy) + rz*(cx*sz - cz*sx*sy), rz*cx*cz*cy - rx*cz*sy + ry*cz*cy*sx, rz*(cz*sx - cx*sz*sy) - ry*(cx*cz + sx*sz*sy) - rx*cy*sz],
                            [- ry*(cz*sx - cx*sz*sy) - rz*(cx*cz + sx*sz*sy), rz*cx*cy*sz - rx*sz*sy + ry*cy*sx*sz, rz*(sx*sz + cx*cz*sy) - ry*(cx*sz - cz*sx*sy) + rx*cz*cy],
                            [                            ry*cx*cy - rz*cy*sx,        - rx*cy - rz*cx*sy - ry*sx*sy,                                          np.float32(0.0)]], dtype = np.float32)

        return deltaF

    
    # Sensor and aiding functions
    def setImuMeasurement(self, imuData, timeImu):
        '''
        Function to set imu data

        Input imu data np shape (6,1) on form [ax ay az wx wy wz]'
        '''

        ## Parsing data
        # Splits data to acc and omg part
        accMeasure = imuData[0:3]
        omgMeasure = imuData[3:6]

        # Transforming data to estimation frame
        accMeasure_e = self.__rotMat_bs @ accMeasure
        self.__accMeasure_e = accMeasure_e

        # If gravity is a state then it is subtracted in the state transition matrix, othervice it is subtracted here
        if self.__gravityState == True:
            acc = accMeasure_e
        else:            
            acc = accMeasure_e - self.__rotMat_bn.T @ self.__gVec

        omg = self.__rotMat_bs @ omgMeasure   


        ## Calculates dt

        # time
        timeImu = np.float64(timeImu)

        # If timeLastImuMsg is zero then time is set to last time, (lazy method of initializations the time)
        if self.__timeLastImuMsg <= 0.01:
            self.__timeLastImuMsg = timeImu

        # calculates dt
        dt = timeImu - self.__timeLastImuMsg

        # if dt is to large, then sets dt to max dt
        if np.abs(dt) > self.__maxDt:
            dt = self.__maxDt

        # Casting dt to float32
        dt = np.float32(dt)
        
        # Sets time now to timeLast
        self.__timeLastImuMsg = timeImu


        ## If delta imu is configured, then IMU data is accumulated
        if self.__deltaImuCum == True and self.__fixedPredictRate == True:
            # Accumulates imu data
            self.__aCum += acc*dt
            self.__wCum += omg*dt
            self.__deltaImuDt += dt
            # Increments counter
            self.__deltaImuCounter += 1     
        else:
            # Sets control param to self variable
            self.__a = acc
            self.__w = omg
            self.__imuPredictDt = dt

        # If predict is not called at a fixed rate, then it is called here
        if self.__fixedPredictRate == False:
            # Calls actions
            self.__predictAndLevel()

    def setPoseMeasurementWithCovariance(self, pose, R, timePose, covCal = True):
        '''
        Function to set pose in ned frame

        Input   pose np shape (4,1) in ned frame
                R np shape (4,4)
                time scalar
        '''

        # Stores time of current msg
        self.__timeNewPoseUpdate = timePose

        # Splits pose to pos and yaw
        pos_n_nb = pose[0:3]
        yaw_nb = pose[3,0]

        # Transforms position measurement to sensor frame
        
        pos_n_ns    = pos_n_nb + self.__rotMat_bn @ self.__pos_b_bs

        # Splits covariance
        rPos = R[0:3, 0:3]
        rYaw = R[3,3]

        ## Sets pose to position filter
        # Calculates covariance
        if covCal == True:
            # Gets theta and cov of theta
            tHeta_nb    = self.__x[9:12]
            rtHeta      = self.__P[9:12,9:12]
            # Calculates cov prop function
            deltaF = self.__rMatBNCovariancePropagation(tHeta_nb, self.__pos_b_bs)
            rPos = rPos + deltaF @ rtHeta @ deltaF.T
        else:
            rPos = rPos

        # Calls update functions

        # if filter is offline then pos is not calculated
        if self.online:
            self.__posEvaluation(pos_n_ns, rPos)

        self.__yawEvaluation(yaw_nb, rYaw)

    # Set filter params
    def setFilterParameters(self, params):
        '''
        Function to set filter parameters
        '''

        # Passes to member variables
        # Measurement uncertainty covariance
        self.__R_pos        = params.rPos*np.eye(3, dtype=np.float32)

        # Gravity
        self.__gravity = params.gravity
        self.__x[17,0] = -self.__gravity

        self.__gVec = np.array([[np.float32(0.0)],[np.float32(0.0)],[-self.__gravity]], dtype = np.float32)

        # Pos threshold
        self.__posThreshold = params.posThreshold
        self.__yawThreshold = params.yawThreshold

        self.__accHighThreshold = params.levelingWindow
        

        # State transition covariance matrix
        Q = np.eye(self.__nStates, dtype = np.float32)
        Q[0:3,0:3]      = params.qPosition*np.eye(3, dtype = np.float32)
        Q[3:6,3:6]      = params.qLinVel*np.eye(3, dtype = np.float32)
        Q[6:9,6:9]      = params.qBiasAcc*np.eye(3, dtype = np.float32)
        Q[9:12, 9:12]   = params.qAngVel*np.eye(3, dtype = np.float32)
        Q[12:15, 12:15] = params.qBiasGyro*np.eye(3, dtype = np.float32)
        Q[15:18, 15:18] = params.qGravity*np.eye(3, dtype = np.float32)

        # Sets to self variable
        self.__Q = Q

        # Sensor to estimation frame transform
        self.__rotMat_bs = params.rotMat_bs
        self.__pos_b_bs = params.pos_b_bs

        # Initial condition
        # Postion and velocity
        self.__x[0:6]   = params.initState[0:6]
        # Acc Bias
        self.__x[6:9]   = params.initState[9:12]
        # tHeta
        self.__x[9:12] = params.initState[12:15]
        # Omg Bias
        self.__x[12:15] = params.initState[18:21]

        self.__xInit = self.__x

        # If second order predict
        if self.__secondOrderPredict == True:
            self.__xLast = self.__x


        # Postion and velocity
        self.__P[0:6,0:6] = params.initCov[0:6,0:6]
        # Acc Bias
        self.__P[6:9,6:9] = params.initCov[9:12,9:12]
        # tHeta
        self.__P[9:12,9:12] = params.initCov[12:15,12:15]
        # Omg Bias
        self.__P[12:15,12:15] = params.initCov[18:21,18:21]

        self.__PInit = self.__P
        
        # Sets to self variables
        self.__timeMaxDiffPose = params.timeMaxDelayPose

        # Filter Config
        self.__deltaImuCum = params.deltaImuCum
        self.__secondOrderPredict = params.secondOrderPredict
        self.__fixedPredictRate = params.fixedRatePredict
    
    # Public get functions
    def getPositionWithCovariance(self, calCov = False):
        '''
        Function to get position in ned frame

        Output  position np shape (3,1)
                rPos     np shape (3,3)
        '''

        # Gets position of sensor
        pos_n_ns = self.__x[0:3]
        rPos = self.__P[0:3,0:3]

        # Gets orientation
        tHeta_nb = self.__x[9:12]
        rtHeta = self.__P[9:12,9:12]

        # Transforms position measurement
        rotMat_bn = self.__droneGeom.rotFun_bn(tHeta_nb)
        pos_n_nb = pos_n_ns - rotMat_bn @ self.__pos_b_bs

        # Calculates covariance
        if calCov == True:
            deltaF = self.__rMatBNCovariancePropagation(tHeta_nb, self.__pos_b_bs)
            rTot = rPos + deltaF @ rtHeta @ deltaF.T
        else:
            rTot = rPos

        return pos_n_nb, rTot

    def getOrientationWithCovariance(self):
        '''
        Function to get orientation

        Output  orientation np shape (3,1)
                rOri        np shape (3,3)
        '''

        return self.__x[9:12], self.__P[9:12,9:12]

    def getLinearVelocityBodyWithCovariance(self, covCal = False):
        '''
        Function to get linear velocity in body frame

        This function is a combination of the linear velocity from posFilter, and the angular velocity of the oriFilter

        Output:     vel_b_nb np shape (3,1)
                    rBody    np shape (3,3)
        '''

        # Gets states to use for calculation
        # Gets linear velocity in ned frame
        vel_n_ns = self.__x[3:6]

        # Transforms to body
        rotMat_nb = self.__rotMat_bn.T
        vel_b_ns = rotMat_nb @ vel_n_ns

        # Transforms covariance
        rVel_n = self.__P[3:6,3:6]
        rVel_b = rotMat_nb @ rVel_n @ rotMat_nb.T

        # Calculating covariance
        if covCal:
            # Gets variables
            rOmg = self.__P[12:15,12:15]
            pos = self.__pos_b_bs.copy()
            omg_b_ns = self.__w - self.__x[12:15]
            # propper vel
            vel_b_nb = vel_b_ns - self.__skew(omg_b_ns) @ pos
            # propper cov
            deltaFMat = self.__skew(self.__pos_b_bs)
            rTot = rVel_b + deltaFMat @ rOmg @ deltaFMat.T
        else:
            # Dirty vel
            rTot = rVel_b
            vel_b_nb = vel_b_ns

        return vel_b_nb, rTot

    def getAngularVelocityBodyWithCovariance(self):
        '''
        Function to get angular velocity of body frame

        Output:     omg_b_nb np shape (3,1)
                    rBody    np shape (3,3)
        '''

        return self.__w - self.__x[12:15], self.__P[12:15,12:15]

    def getLinearVelocityLevelWithCovariance(self, covCal = False):
        '''
        Function to get linear velocity in level frame

        This function is a combination of the linear velocity from posFilter, and the angular velocity of the oriFilter

        Output:     vel_n_nb np shape (3,1)
                    rLevel   np shape (3,3)
        '''

        # Gets states to use for calculation
        vel_b_nb, rBody = self.getLinearVelocityBodyWithCovariance(covCal = covCal)
        
        # Transforms to level frame
        theta_nb = self.__x[9:12]
        rotMat_bl = self.__droneGeom.rotFun_bl(theta_nb)

        vel_l_nb = rotMat_bl @ vel_b_nb

        # Calculating covariance
        if covCal == True:
            deltaF = rotMat_bl
            rLevel = deltaF @ rBody @ deltaF.T
        else:
            rLevel = rBody

        return vel_l_nb, rLevel

    def getAngularVelocityLevelWithCovariance(self, covCal = False):
        '''
        Function to get angular velocity of level frame

        Output:     omg_b_nb np shape (3,1)
                    rLevel   np shape (3,3)
        '''

        # Gets angular velocity
        omg_b_nb = self.__w - self.__x[12:15]
        rOmg_b = self.__P[12:15,12:15]

        # Transforms to level frame
        theta_nb = self.__x[9:12]
        rotMat_bl = self.__droneGeom.rotFun_bl(theta_nb)

        # Transforms rates and cov
        omg_l_nb = rotMat_bl @ omg_b_nb

        if covCal == True:
            rOmg_l = rotMat_bl @ rOmg_b @ rotMat_bl.T
        else:
            rOmg_l = rOmg_b

        return omg_l_nb, rOmg_l

    def getPosFilterOnlineState(self):
        '''
        Function to check if kalman filter is online or not
        '''
        
        return self.online

    def getAccBiasWithCovariance(self):
        '''
        Function to get accelerometer bias
        '''

        return self.__x[6:9], self.__P[6:9, 6:9]

    def getGyroBiasWithCovariance(self):
        '''
        Function to get gyro bias
        '''

        return self.__x[12:15], self.__P[12:15, 12:15]

    # Jit Init function
    def resetFilter(self, keepAngles = False):
        '''
        Function to reset all parameters after runing JitInit function
        '''

        # Reseting state and state covariance
        if keepAngles == True:
            # In some cases there is only a need to reset the linear states, then angular states is kept
            self.__x[0:9] = self.__xInit[0:9]
            self.__P[0:9, 0:9] = self.__PInit[0:9, 0:9]
            self.__x[11] = self.__xInit[11]
            self.__P[11,11] = self.__PInit[11,11]
            self.__x[15:18] = self.__xInit[15:18]
            self.__P[15:18] = self.__PInit[15:18]
            # Resets last variables
            if self.__secondOrderPredict:
                self.__xLast = self.__x
                self.__aLast = np.zeros((3,1), np.float32)
                self.__wLast = np.zeros((3,1), np.float32)
        else:
            self.__x = self.__xInit
            self.__P = self.__PInit
            # Resets last variables
            if self.__secondOrderPredict:
                self.__xLast = self.__x
                self.__aLast = np.zeros((3,1), np.float32)
                self.__wLast = np.zeros((3,1), np.float32)
        
        # Resets time stuff
        self.__timeNewPoseUpdate = 0.0
        self.__timeDiffPose = self.__timeMaxDiffPose +2.0

    def __dryRun(self):
        '''
        Function to call all functions in the class
        '''

        # Predict function
        self.predict()
        dummyTimeF64 = np.float64(0.0)
        self.checkTiming(dummyTimeF64)
        dummyDt = np.float32(0.0)
        self.__predict(dummyDt)
        self.__stateTransitionLin(dummyDt)
        dummyVec3 = np.ones((3,1), dtype = np.float32)
        self.__velThetaStateTransTherm(dummyVec3, dummyVec3, dummyVec3)
        self.__thetaThetaStateTransTherms(dummyVec3, dummyVec3, dummyVec3)
        self.__controlInput(dummyDt)
        self.__covariancePredictWeight(dummyDt)
        self.__wrapEulerAngles()
        self.__modPiToPi(np.float32(0.0))
        # Innovation lin
        dummyXMeasure = np.zeros((1,1), dtype = np.float32)
        dummyHmat = np.zeros((1,self.__nStates), dtype = np.float32)
        dummyHmat[0,0] = 1.0
        dummyY = self.__innovationLin(dummyXMeasure,dummyHmat)
        # InnovationYaw
        self.__innovationYaw(0.0, 0.0)
        # InnovationPi
        self.__innovationPhiTheta(0.0, 0.0)
        # KalmanCompute
        dummyR = np.eye(1, dtype = np.float32)
        dummyK = self.__computeKalman(dummyHmat, dummyR)
        # Update
        self.__update(dummyK, dummyY, dummyHmat)
        # AngleLeveling
        dummyAcc = np.zeros((3,1), dtype = np.float32)
        self.__angleLeveling(dummyAcc)
        # YawMeasure
        rYaw = np.float32(1.0)
        self.__yawMeasurement(0.0, rYaw)
        self.__yawSet(0.0)
        self.__yawEvaluation(0.0, rYaw)
        # PosMeasure
        dummyPos = np.zeros((3,1), dtype = np.float32)
        dummyR = np.eye(3, dtype = np.float32)
        self.__posMeasurement(dummyPos, dummyR)
        self.__posSet(dummyPos)
        self.__posEvaluation(dummyPos, dummyR)
        # SetImuMeasurement
        dummyIMU = np.zeros((6,1), dtype = np.float32)
        dummyTime = np.float32(0.0)
        self.setImuMeasurement(dummyIMU, dummyTime)
        # SetPosMeasurement
        dummyPose = np.zeros((4,1), dtype = np.float32)
        dummyR = np.zeros((4,4), dtype = np.float32)
        self.setPoseMeasurementWithCovariance(dummyPose,dummyR, dummyTime)
        # Skew function
        dummyVec = np.zeros((3,1), dtype = np.float32)
        self.__skew(dummyVec)
        # Cov function
        self.__rMatBNCovariancePropagation(dummyVec, dummyVec)
        # Set Filter parameters
        dummyFilterParams = InsParameters()
        self.setFilterParameters(dummyFilterParams)
        # Get Position
        self.getPositionWithCovariance()
        # Get orientation
        self.getOrientationWithCovariance()
        # Get linear velocity
        self.getLinearVelocityBodyWithCovariance()
        self.getLinearVelocityLevelWithCovariance()
        # Get Angular velocity
        self.getAngularVelocityBodyWithCovariance()
        self.getAngularVelocityLevelWithCovariance()

        self.getPosFilterOnlineState

    def jitInit(self):
        '''
        Function to call all functions in the class

        This function should be ran right after setting up the class, this is tu ensure that everything is JIT compiled before run time
        '''

        # INS main class
        print('DryRun: INS Main')
        self.__dryRun()
        self.resetFilter()



def main():
    pass



if __name__ == '__main__':
    main()
    