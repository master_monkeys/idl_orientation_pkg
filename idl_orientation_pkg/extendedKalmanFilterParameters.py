
import numpy as np


# Ekf parameter struct (class :o )
class InsParameters(object):
    
    def __init__(self):

        ## System state predict uncertainty matrix
        self.qPosition   = 1.0
        self.qAngles     = 1.0
        self.qLinVel     = 1.0
        self.qAngVel     = 1.0
        self.qLinAcc     = 1.0
        self.qAngAcc     = 1.0
        self.qBiasAcc    = 1.0
        self.qBiasGyro   = 1.0
        self.qGravity    = 1.0

        ## Measurement uncertainties covariance
        self.rYaw   = 1.0
        self.rPos   = 1.0
        self.rAcc   = 1.0
        self.rGyro  = 1.0
        self.rLevel = 1.0

        ## Acceleration related
        self.levelingWindow = 1.0
        self.gravity = 9.81

        ## Position thresholding
        self.posThreshold = 1.0
        self.yawThreshold = 1.57

        ## Sensor orientation relative to estimation frame orientation(Ned)
        self.rotMat_bs = np.eye(3, dtype = np.float32)
        self.pos_b_bs = np.ones((3,1), dtype = np.float32)

        ## Initial conditions
        self.nStates = 21
        self.initState =  np.zeros((self.nStates,1), dtype = np.float32)
        self.initCovVec = np.ones((self.nStates,1), dtype = np.float32)
        self.initCov = np.diag(self.initCovVec[:,0])

        # Time
        self.timeMaxDelayPose = 10.0

        # Filter config
        self.deltaImuCum = True
        self.secondOrderPredict = True
        self.fixedRatePredict = True


def main():
    pass


if __name__ == '__main__':
    main()
