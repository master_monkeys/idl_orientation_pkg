

import numpy as np


# Import Drone config class
from idl_botsy_pkg.JITdroneConfiguration import DroneGeometry

# Imports Filters and filter params
from idl_orientation_pkg.JITextendedKalmanFilterParameters import InsParameters

# Chose if we are going to use filter with sensor as control signal or measurement
Control = True
if Control == True:
    from idl_orientation_pkg.JITextendedKalmanFilterPositionControl import InsPosition
    from idl_orientation_pkg.JITextendedKalmanFilterOrientationControl import InsOrientationAtan
else:
    from idl_orientation_pkg.JITextendedKalmanFilterPosition import InsPosition
    from idl_orientation_pkg.JITextendedKalmanFilterOrientation import InsOrientationAtan


# import Numba stuff
from numba.experimental import jitclass
from numba import int32, float32, float64, boolean, jit, types, typed, typeof  # import the types


# Better EKF class implementation
# Implementing orientation and position as separate filters


# Numba specs for Ins EKF
orientationFilterType = InsOrientationAtan.class_type.instance_type
positionFilterType = InsPosition.class_type.instance_type
droneGeomNumbaType = DroneGeometry.class_type.instance_type
InsEkfSpec = [
    ('__orientationFilter', orientationFilterType),
    ('__positionFilter', positionFilterType),
    ('__droneGeom', droneGeomNumbaType),
    ('__pos_b_bs', float32[:,:]),
    ('__timeNewPoseUpdate', float64),
    ('__timeMaxDiffPose', float64),
    ('__timeDiffPose', float64),
    ('__timeLastImuMsg', float64),
    ('__maxDt', float64),
    ('__filterIsNotReSet', boolean),
]

# Holistic INS ekf class
@jitclass(InsEkfSpec)
class InsEkf(object):

    def __init__(self, dt):

        # Creates orientation and position filtering objects
        self.__orientationFilter = InsOrientationAtan(dt)

        self.__positionFilter = InsPosition(dt)

        # Geometry data
        self.__droneGeom = DroneGeometry()
        self.__pos_b_bs = np.zeros((3,1), dtype = np.float32)

        # Time stuff
        self.__timeMaxDiffPose = 10.0
        self.__timeNewPoseUpdate = 0.0
        self.__timeDiffPose = self.__timeMaxDiffPose +2.0

        self.__timeLastImuMsg = 0.0
        self.__maxDt = 1.0/50.0

        # Bools
        self.__filterIsNotReSet = True

        print('INS split alive!')

    def predict(self):
        '''
        Function to call predict function of both part filters at the same time
        '''

        # Calls both predict functions
        self.__orientationFilter.predict(online = self.__positionFilter.online)
        self.predictPosition()

    def checkTiming(self, currentTime):
        '''
        Function to routinely call to check if timers has expired
        '''

        # Finds delta time            
        self.__timeDiffPose = currentTime - self.__timeNewPoseUpdate

        # If timer expires, then filter is set offline
        if self.__timeDiffPose > self.__timeMaxDiffPose:
            self.__positionFilter.online = False
            # Reseting filter once
            if self.__filterIsNotReSet == True:
                self.__filterIsNotReSet = False
                self.resetFilter()
                # Printing that filter is offline
                print('Warn: orientation node: pos filter offline')
                print('timer expired by:')
                print(self.__timeDiffPose)
        else:
            self.__positionFilter.online = True
            # Reseting filter once
            if self.__filterIsNotReSet == False:
                self.__filterIsNotReSet = True
                print('Status: orientation node: pos filter online!')

    def predictOrientation(self, online = True):
        '''
        Function to predict orientation filter
        '''

        self.__orientationFilter.predict(online = online)

    def predictPosition(self):
        '''
        Function to predict position
        '''
        
        # Predicts
        self.__positionFilter.predict()

    def __skew(self, vec):
        '''
        Function to return matrix form of cross product

        Output:     skew mat np shape (3,1)
        '''
        x = vec[0,0]
        y = vec[1,0]
        z = vec[2,0]

        skew = np.array([   [ 0,-z, y],
                            [ z, 0,-x],
                            [-y, x, 0]], dtype = np.float32)

        return skew

    def __rMatBNCovariancePropagation(self, tHeta, pos):
        '''
        Function to return the derivative of the propagation function

        Input       tHeta np shape (3,1)
                    pos   np shape (3,1)
        '''
        # Parsing data
        phi     = tHeta[0,0]
        theta   = tHeta[1,0]
        psi     = tHeta[2,0]

        rx = pos[0,0]
        ry = pos[1,0]
        rz = pos[2,0]

        # Pre computing
        sx = np.sin(phi)
        cx = np.cos(phi)
        sy = np.sin(theta)
        cy = np.cos(theta)
        sz = np.sin(psi)
        cz = np.cos(psi)

        deltaF = np.array([ [  ry*(sx*sz + cx*cz*sy) + rz*(cx*sz - cz*sx*sy), rz*cx*cz*cy - rx*cz*sy + ry*cz*cy*sx, rz*(cz*sx - cx*sz*sy) - ry*(cx*cz + sx*sz*sy) - rx*cy*sz],
                            [- ry*(cz*sx - cx*sz*sy) - rz*(cx*cz + sx*sz*sy), rz*cx*cy*sz - rx*sz*sy + ry*cy*sx*sz, rz*(sx*sz + cx*cz*sy) - ry*(cx*sz - cz*sx*sy) + rx*cz*cy],
                            [                            ry*cx*cy - rz*cy*sx,        - rx*cy - rz*cx*sy - ry*sx*sy,                                          np.float32(0.0)]], dtype = np.float32)

        return deltaF

    # Sensor and aiding functions
    def setImuMeasurement(self, imuData, timeImu):
        '''
        Function to set imu data

        Input imu data np shape (6,1) on form [ax ay az wx wy wz]'
        '''

        ## Parsing data
        # Splits data to acc and omg part
        accMeasure = imuData[0:3]
        omgMeasure = imuData[3:6]

        # time
        timeImu = np.float64(timeImu)

        # Calculates dt

        # If timeLastImuMsg is zero then time is set to last time
        if self.__timeLastImuMsg <= 0.01:
            self.__timeLastImuMsg = timeImu

        # calculates dt
        dt = timeImu - self.__timeLastImuMsg

        # if dt is to large, then sets dt to max dt
        if dt > self.__maxDt:
            dt = self.__maxDt

        # Casting dt to float32
        dt = np.float32(dt)
        
        # Sets time now to timeLast
        self.__timeLastImuMsg = timeImu

        # Gets imu bias
        accBias = self.__positionFilter.getAccBias()

        # Sets data to orientation filter
        accNoBias = accMeasure
        self.__orientationFilter.setImuMeasurement(accNoBias, omgMeasure, dt, online = self.__positionFilter.online)

        # Sets data to position filter 
        # Gets tHeta from orientation filter and sets it to the position filter
        tHeta, tHetaCov = self.__orientationFilter.getOrientationWithCovariance()
        self.__positionFilter.setOrientation(tHeta, tHetaCov)
              
        self.__positionFilter.setAccMeasurement(accMeasure, dt)

    def setPoseMeasurementWithCovariance(self, pose, R, timePose, covCal = True):
        '''
        Function to set pose in ned frame

        Input   pose np shape (4,1) in ned frame
                R np shape (4,4)
                time scalar
        '''

        # Stores time of current msg
        self.__timeNewPoseUpdate = timePose

        # Splits pose to pos and yaw
        pos_n_nb = pose[0:3]
        yaw_nb = pose[3,0]

        # Transforms position measurement to sensor frame
        tHeta_nb, rtHeta = self.__orientationFilter.getOrientationWithCovariance()
        rotMat_bn = self.__droneGeom.rotFun_bn(tHeta_nb)
        pos_n_ns = pos_n_nb + rotMat_bn @ self.__pos_b_bs

        # Splits covariance
        rPos = R[0:3, 0:3]
        rYaw = R[3,3]

        ## Sets pose to position filter
        # Calculates covariance
        if covCal:
            deltaF = self.__rMatBNCovariancePropagation(tHeta_nb,self.__pos_b_bs)
            rPosTot = rPos + deltaF @ rtHeta @ deltaF.T

        self.__positionFilter.setPosMeasurementWithCovariance(pos_n_ns, rPosTot)

        # Sets yaw to orientation filter
        self.__orientationFilter.setYawMeasurementWithCovariance(yaw_nb, rYaw, online = self.__positionFilter.online)

    # Set filter params
    def setFilterParameters(self, params):
        '''
        Function to set filter parameters
        '''

        # Passes to member objects
        self.__orientationFilter.setFilterParameters(params)
        self.__positionFilter.setFilterParameters(params)

        # Sets to self variables
        self.__pos_b_bs = params.pos_b_bs
        self.__timeMaxDiffPose = params.timeMaxDelayPose

    # Reset filter
    def resetFilter(self, keepAngles = False):
        '''
        Function to set filters to init values set in insParams
        '''

        self.__positionFilter.resetFilter()
        self.__orientationFilter.resetFilter(keepAngles = keepAngles)
        print('warn: ros_node_ekf: filters reset')

    # Public get functions
    def getPositionWithCovariance(self, covCal = False):
        '''
        Function to get position in ned frame

        Output  position np shape (3,1)
                rPos     np shape (3,3)
        '''

        # Gets position of sensor
        pos_n_ns, rPos = self.__positionFilter.getPositionWithCovariance()

        # Gets orientation
        tHeta_nb, rtHeta = self.__orientationFilter.getOrientationWithCovariance()

        # Transforms position measurement
        rotMat_bn = self.__droneGeom.rotFun_bn(tHeta_nb)
        pos_n_nb = pos_n_ns - rotMat_bn @ self.__pos_b_bs

        # Calculates covariance
        if covCal == True:
            deltaF = self.__rMatBNCovariancePropagation(tHeta_nb, self.__pos_b_bs)
            rTot = rPos + deltaF @ rtHeta @ deltaF.T
        else:
            rTot = rPos

        return pos_n_nb, rTot

    def getOrientationWithCovariance(self):
        '''
        Function to get orientation

        Output  orientation np shape (3,1)
                rOri        np shape (3,3)
        '''

        return self.__orientationFilter.getOrientationWithCovariance()

    def getLinearVelocityBodyWithCovariance(self, covCal = False):
        '''
        Function to get linear velocity in body frame

        This function is a combination of the linear velocity from posFilter, and the angular velocity of the oriFilter

        Output:     vel_n_nb np shape (3,1)
                    rBody    np shape (3,3)
        '''

        # Gets states to use for calculation
        omg_b_ns, rOmg = self.__orientationFilter.getAngularVelocityWithCovariance()
        vel_b_ns, rVel = self.__positionFilter.getLinearVelocityBodyWithCovariance(covCal = covCal)

        

        # Calculating covariance
        if covCal == True:
            pos = self.__pos_b_bs.copy()

            # Calculating velocity
            vel_b_bn = vel_b_ns - self.__skew(omg_b_ns) @ pos

            deltaFMat = self.__skew(self.__pos_b_bs)
            rTot = rVel + deltaFMat @ rOmg @ deltaFMat.T
        else:
            vel_b_bn = vel_b_ns
            rTot = rVel

        return vel_b_bn, rTot

    def getAngularVelocityBodyWithCovariance(self):
        '''
        Function to get angular velocity of body frame

        Output:     omg_b_nb np shape (3,1)
                    rBody    np shape (3,3)
        '''

        return self.__orientationFilter.getAngularVelocityWithCovariance()

    def getLinearVelocityLevelWithCovariance(self, covCal = False):
        '''
        Function to get linear velocity in level frame

        This function is a combination of the linear velocity from posFilter, and the angular velocity of the oriFilter

        Output:     vel_n_nb np shape (3,1)
                    rLevel   np shape (3,3)
        '''

        # Gets states to use for calculation
        vel_b_nb, rBody = self.getLinearVelocityBodyWithCovariance(covCal = covCal)
        

        # Transforms to level frame
        theta_nb, _ = self.__orientationFilter.getOrientationWithCovariance()
        rotMat_bl = self.__droneGeom.rotFun_bl(theta_nb)

        vel_l_nb = rotMat_bl @ vel_b_nb

        # Calculating covariance
        if covCal == True:
            deltaF = rotMat_bl
            rLevel = deltaF @ rBody @ deltaF.T
        else:
            rLevel = rBody

        return vel_l_nb, rLevel

    def getAngularVelocityLevelWithCovariance(self, covCal = False):
        '''
        Function to get angular velocity of level frame

        Output:     omg_b_nb np shape (3,1)
                    rLevel   np shape (3,3)
        '''

        # Gets angular velocity
        omg_b_nb, rOmg_b = self.__orientationFilter.getAngularVelocityWithCovariance()

        # Transforms to level frame
        theta_nb, _ = self.__orientationFilter.getOrientationWithCovariance()
        rotMat_bl = self.__droneGeom.rotFun_bl(theta_nb)

        # Transforms rates and cov
        omg_l_nb = rotMat_bl @ omg_b_nb

        if covCal == True:
            rOmg_l = rotMat_bl @ rOmg_b @ rotMat_bl.T
        else:
            rOmg_l = rOmg_b

        return omg_l_nb, rOmg_l

    def getPosFilterOnlineState(self):
        '''
        Function to check if kalman filter is online or not
        '''
        
        return self.__positionFilter.online

    def getAccBiasWithCovariance(self):
        '''
        Function to get accelerometer bias
        '''

        return self.__positionFilter.getAccBiasWithCovariance()

    def getGyroBiasWithCovariance(self):
        '''
        Function to get gyro bias
        '''

        return self.__orientationFilter.getGyroBiasWithCovariance()

    # Jit Init function
    def __resetFilter(self):
        '''
        Function to reset all parameters after runing JitInit function
        '''
        
        # Resets time stuff
        self.__timeNewPoseUpdate = 0.0
        self.__timeDiffPose = self.__timeMaxDiffPose +2.0

    def __dryRun(self):
        '''
        Function to call all functions in the class
        '''

        # Predict function
        self.predict()
        dummyTimeF64 = np.float32(0.0)
        self.checkTiming(dummyTimeF64)
        # Skew function
        dummyVec = np.zeros((3,1), dtype = np.float32)
        self.__skew(dummyVec)
        # Cov function
        self.__rMatBNCovariancePropagation(dummyVec, dummyVec)
        # Set ImuMeasurement
        dummyImuData = np.zeros((6,1), dtype = np.float32)
        dummyTime = np.float32(0.0)
        self.setImuMeasurement(dummyImuData, dummyTime)
        # SetPosMeasurement
        dummyPose = np.zeros((4,1), dtype = np.float32)
        dummyR = np.zeros((4,4), dtype = np.float32)
        self.setPoseMeasurementWithCovariance(dummyPose,dummyR, dummyTime)
        # Set Filter parameters
        dummyFilterParams = InsParameters()
        self.setFilterParameters(dummyFilterParams)
        # Get Position
        self.getPositionWithCovariance()
        # Get orientation
        self.getOrientationWithCovariance()
        # Get linear velocity
        self.getLinearVelocityBodyWithCovariance()
        self.getLinearVelocityLevelWithCovariance()
        # Get Angular velocity
        self.getAngularVelocityBodyWithCovariance()
        self.getAngularVelocityLevelWithCovariance()

    def jitInit(self):
        '''
        Function to call all functions in the class

        This function should be ran right after setting up the class, this is tu ensure that everything is JIT compiled before run time
        '''

        # INS main class
        print('DryRun: INS Main')
        self.__dryRun()
        self.__resetFilter()

        # INS orientation
        print('DryRun: INS Ori')
        self.__orientationFilter.jitInit()

        # INS position
        print('DryRun: INS Pos')
        self.__positionFilter.jitInit()



def main():
    pass



if __name__ == '__main__':
    main()
    